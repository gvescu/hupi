SimpleCov.start do
  add_filter 'bin/'
  add_filter 'config/'
  add_filter 'test/'

  add_group 'Controllers', 'app/controllers'
  add_group 'Helpers', 'app/helpers'
  add_group 'Models', 'app/models'
  add_group 'Libraries', 'lib/'
end
