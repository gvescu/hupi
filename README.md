# HUPI

[![trello](https://img.shields.io/badge/trello--blue.svg)](https://trello.com/b/wMmLmv1I/projeto-hupi)
[![PRD](https://img.shields.io/badge/PRD--blue.svg)](https://docs.google.com/document/d/1RKS8CrmIiiz-JC3Pd7VABjspL490tA8Kox_G7Ll1uZg/edit)

- [Dependencies](#dependencies)
- [Setup](#setup)
- [Development](#development)
- [Staging](#staging)
- [Style](#style)

## Dependencies

- Ruby 2.3.3
- PostgreSQL 9.6.1
- PhantomJS 1.8.1
- Redis 3.0.4

```sh
brew update
brew install postgres phantomjs redis
```

## Setup

```sh
git clone git@gitlab.com:magrathealabs/hupi.git
cd hupi
bundle install
cp config/database.yml.example config/database.yml
bin/rails db:setup
```

## Development

Run the local server at http://localhost:3000 with:

```sh
bin/rails s
```

Check code style with:

```sh
rubocop
```

Run tests with:

```sh
bin/rails t
```

Use guard to run tests and check code style as you code:

```sh
guard
```

You can check test coverage information by running the test suite and looking into `coverage/` folder:

```sh
bin/rails test
open coverage/index.html
```

Test mailers manually using `mailcatcher`:

```sh
# With RVM
rvm default@mailcatcher --create do gem install mailcatcher
rvm wrapper default@mailcatcher --no-prefix mailcatcher catchmail

# Without RVM
gem install mailcatcher

sidekiq
mailcatcher
```

## Staging

We're using Heroku as Staging environment. Take a look in the
[documentation](https://devcenter.heroku.com/articles/getting-started-with-rails5) to setup Heroku in your machine.

```sh
heroku login
heroku create
git push heroku master
heroku ps:scale web=1
heroku config:set AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID> AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
heroku config:set S3_BUCKET_NAME=<bucket-name>
heroku run rake db:migrate
heroku open
```

## Style

- [Ruby style guide](https://github.com/bbatsov/ruby-style-guide)
- [Rails style guide](https://github.com/bbatsov/rails-style-guide)
- [JavaScript style guide](https://github.com/airbnb/javascript)
