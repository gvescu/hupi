require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Hupi
  class Application < Rails::Application
    config.i18n.default_locale = :'pt-BR'
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.eager_load_paths += ["#{Rails.root}/lib"]
    config.active_job.queue_adapter = :sidekiq

    # Add the font path
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')        

    # Include font files to Assets
    config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/

    # Load Videos Assets
    config.assets.paths << "#{Rails.root}/app/assets/videos"

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
