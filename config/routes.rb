Rails.application.routes.draw do
  root to: 'landing_page#home'

  get 'legal/termos_de_uso', to: 'landing_page#terms_of_service'
  get 'legal/privacidade', to: 'landing_page#privacy'
  get 'proposito', to: 'landing_page#purpose'
  get 'nossos-diferenciais', to: 'landing_page#differential'
  get 'pontos-educacao', to: 'landing_page#education_points'
  get 'nuestros-diferenciales', to: 'landing_page#differential'
  get 'puntos-educacion', to: 'landing_page#education_points'
  get 'futuros-lancamentos', to: 'landing_page#future_releases'
  get 'futuros-lanzamientos', to: 'landing_page#future_releases'
  get 'simulacao', to: 'landing_page#simulation'
  get 'simulacion', to: 'landing_page#simulation'
  post 'simulacao', to: 'landing_page#simulator'  
  get 'tour-360', to: 'landing_page#tour'  
  get 'sustentabilidade', to: 'landing_page#sustainability'

  get 'callback/document_signature', to: 'shop/contracts#callback'

  post 'webhooks/documents', to: 'webhook/documents#callback'

  localized do
    devise_for :users, only: :sessions, controllers: { sessions: :sessions }
    devise_for :users, only: :sessions, path: :admin

    unauthenticated :user do
      resources :document_downloads do         
        collection do 
          get :donwload_video
        end  
      end  
    end  

    authenticate :user do
      resources :document_downloads, only: [:show] do 
        member do 
          get :donwload_contract          
        end 

        collection do 
          get :donwload_video
        end  
      end  

      scope module: 'customer' do
        resource :contract, only: :show
        resource :user, only: [:edit, :update]
        resource :rg_document, only: [:edit, :update]
        resource :residence_document, only: [:edit, :update]
        resource :employment_document, only: [:edit, :update]
      end

      scope module: :shop do
        resource :bloc, only: [:edit, :update] do 
          get :back
        end 
        resource :pavement, only: [:edit, :update]
        resource :apartment, only: [:edit, :update]
        resources :apartments, only: [:queue] do 
          get :queue
        end    
        resource :document, only: [:edit, :update]
        resource :analyze, only: [:show]
        resource :contract, only: [:edit, :update]
        resource :analyze_contract, only: [:show]
        resource :analyze_bank, only: [:show]
      end

      scope :admin, as: 'admin', module: 'admin' do
        resources :contracts, only: :create
        resources :users, only: [:index, :show, :edit, :update, :destroy] do
          post :approved, on: :member
          post :approved_contract, on: :member   
          post :approved_contract_document, on: :member   
          post :signature_bank, on: :member   
          delete :refuse, on: :member  
          delete :refuse_contract, on: :member   
          delete :refuse_contract_document, on: :member   
          delete :refuse_bank, on: :member       
          
          post :reset_account, on: :member  

          resource :document, only: [:edit, :update]          
        end
      end
    end
  end
end
