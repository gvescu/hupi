if Rails.env.test? || Rails.env.development?
  CarrierWave.configure do |config|
    config.enable_processing = false
    #config.root = "#{Rails.root}/tmp/uploads"    
    config.storage = :file
  end
else
  CarrierWave.configure do |config|
    config.fog_credentials = {
      provider: 'AWS',
      aws_access_key_id: ENV['AWS_ACCESS_KEY_ID'],
      aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      region: 'sa-east-1'
    }
    config.storage = :fog
    config.fog_provider = 'fog/aws'
    config.fog_directory = "hupi-assets-#{Rails.env}"
    config.cache_dir = "#{Rails.root}/tmp/uploads"
  end
end
