require 'facebook/account_kit'

Facebook::AccountKit.config do |config|
  config.account_kit_version = 'v1.1'
  config.account_kit_app_secret = Rails.application.secrets.account_kit_app_secret
  config.facebook_app_id = Rails.application.secrets.facebook_app_id
end
