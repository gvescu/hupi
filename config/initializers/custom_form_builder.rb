class CustomFormBuilder < ActionView::Helpers::FormBuilder
  def error_notification
    return unless object.respond_to?(:errors) && object.errors.any?

    @template.content_tag :div, class: 'alert alert-danger' do
      @template.fa_icon_tag('remove') + ' ' +
        I18n.t('activerecord.errors.messages.not_saved', count: object.errors.count)
    end
  end
end

ActionView::Base.default_form_builder = CustomFormBuilder
