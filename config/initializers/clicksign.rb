require 'clicksign'

Clicksign.configure do |config|
  config.token = ENV['CLICKSIGN_TOKEN']
  config.endpoint = ENV['CLICKSIGN_URL']
end