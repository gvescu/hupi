# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180731004517) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "number",                              null: false
    t.string   "street",                 default: "", null: false
    t.string   "zip_code",     limit: 9, default: "", null: false
    t.string   "complement",             default: ""
    t.string   "neighborhood",           default: ""
    t.string   "city",                   default: "", null: false
    t.string   "state",                  default: "", null: false
    t.integer  "user_id",                             null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["user_id"], name: "index_addresses_on_user_id", using: :btree
  end

  create_table "documents", force: :cascade do |t|
    t.string   "file"
    t.string   "type"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_documents_on_user_id", using: :btree
  end

  create_table "purchases", force: :cascade do |t|
    t.string   "bloc"
    t.string   "pavement"
    t.string   "apartment"
    t.datetime "received"
    t.datetime "analyze"
    t.datetime "approved"
    t.datetime "disapproved"
    t.datetime "contact"
    t.datetime "signature"
    t.integer  "current_step", default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                     default: "",    null: false
    t.string   "encrypted_password",                        default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                             default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.string   "name"
    t.string   "rg"
    t.string   "cpf",                            limit: 11
    t.string   "pis",                            limit: 11
    t.datetime "birthdate"
    t.bigint   "family_income"
    t.integer  "marital_status",                            default: 0,     null: false
    t.string   "phone"
    t.boolean  "admin",                                     default: false
    t.integer  "current_step",                              default: 1,     null: false
    t.integer  "status",                                    default: 0,     null: false
    t.string   "account_kit_id",                            default: "",    null: false
    t.datetime "deleted_at"
    t.string   "bloc"
    t.string   "pavement"
    t.string   "apartment"
    t.datetime "received"
    t.datetime "analyze"
    t.datetime "approved"
    t.datetime "disapproved"
    t.datetime "contact"
    t.datetime "signature"
    t.datetime "approved_bank"
    t.datetime "contract_refused"
    t.datetime "contract_approved"
    t.datetime "disapproved_bank"
    t.datetime "analyze_contract"
    t.datetime "elaborate_contract"
    t.datetime "contract_documents_disapproved"
    t.datetime "contract_documents_approved"
    t.datetime "signature_bank"
    t.string   "document_key"
    t.string   "signature_contract_user_agent"
    t.string   "signature_contract_remote_ip"
    t.string   "signature_contract_hash"
    t.string   "signature_contract_time"
    t.integer  "signature_contract_accepted"
    t.index ["account_kit_id"], name: "index_users_on_account_kit_id", unique: true, using: :btree
    t.index ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "addresses", "users"
end
