class AddContractDocumentsDisapprovedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :contract_documents_disapproved, :datetime
  end
end
