class AddDocumentKeyToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :document_key, :string
  end
end
