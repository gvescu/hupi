class AddColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      t.string :name
      t.string :rg
      t.string :cpf, limit: 11
      t.string :pis, limit: 11
      t.datetime :birthdate
      t.bigint :family_income
      t.integer :marital_status, null: false, default: 0
      t.integer :data_status, null: false, default: 0
    end
  end
end
