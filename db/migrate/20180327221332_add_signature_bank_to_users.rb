class AddSignatureBankToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :signature_bank, :datetime
  end
end
