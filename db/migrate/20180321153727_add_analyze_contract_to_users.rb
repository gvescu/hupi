class AddAnalyzeContractToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :analyze_contract, :datetime
  end
end
