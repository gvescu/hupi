class AddContractRefusedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :contract_refused, :datetime
  end
end
