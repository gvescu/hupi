class AddDisapprovedBankToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :disapproved_bank, :datetime
  end
end
