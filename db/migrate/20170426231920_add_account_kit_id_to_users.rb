class AddAccountKitIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :account_kit_id, :string, null: false, default: ''
    add_index :users, :account_kit_id, unique: true
  end
end
