class AddFieldsContractToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :signature_contract_user_agent, :string
    add_column :users, :signature_contract_remote_ip, :string
    add_column :users, :signature_contract_hash, :string
    add_column :users, :signature_contract_time, :string
    add_column :users, :signature_contract_accepted, :integer
  end
end
