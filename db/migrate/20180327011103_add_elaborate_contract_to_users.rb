class AddElaborateContractToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :elaborate_contract, :datetime
  end
end
