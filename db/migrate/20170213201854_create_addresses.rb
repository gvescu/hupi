class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.integer :number, null: false
      t.string :street, null: false, default: ''
      t.string :zip_code, null: false, limit: 9, default: ''
      t.string :complement, default: ''
      t.string :neighborhood, default: ''
      t.string :city, null: false, default: ''
      t.string :state, null: false, default: ''
      t.references :user, null: false, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
