class AddContractDocumentsApprovedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :contract_documents_approved, :datetime
  end
end
