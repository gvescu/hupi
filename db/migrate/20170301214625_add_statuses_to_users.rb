class AddStatusesToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      t.integer :rg_document_status, null: false, default: 0
      t.integer :residence_document_status, null: false, default: 0
      t.integer :employment_document_status, null: false, default: 0
    end
  end
end
