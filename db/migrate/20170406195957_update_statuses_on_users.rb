class UpdateStatusesOnUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :data_status, :integer, null: false, default: 0
    remove_column :users, :rg_document_status, :integer, null: false, default: 0
    remove_column :users, :residence_document_status, :integer, null: false, default: 0
    remove_column :users, :employment_document_status, :integer, null: false, default: 0
    add_column :users, :status, :integer, null: false, default: 0
  end
end
