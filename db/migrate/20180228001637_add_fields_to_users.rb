class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :bloc, :string
    add_column :users, :pavement, :string
    add_column :users, :apartment, :string
    add_column :users, :received, :datetime
    add_column :users, :analyze, :datetime
    add_column :users, :approved, :datetime
    add_column :users, :disapproved, :datetime
    add_column :users, :contact, :datetime
    add_column :users, :signature, :datetime
  end
end
