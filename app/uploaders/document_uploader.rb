class DocumentUploader < CarrierWave::Uploader::Base  

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end  

  def extension_whitelist
    %w(jpg jpeg png pdf)
  end

  def default_url(*args)
    ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  end
  
  def filename(uploaded_file = file)
    if uploaded_file.present?
      if model.type == 'ContractSignature'                
        "#{model.user.signature_contract_hash}-#{model.user.signature_contract_time}.#{uploaded_file.extension}"      
      else
        "#{uploaded_file.filename}"      
      end          
    end
  end
end
