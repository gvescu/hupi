class ShopMailer < ApplicationMailer
  def send_refuse_document client
    @client = client
    
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end  
  end

  def send_accept_document client
    @client = client
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end  
  end

  def send_refuse_contract client
    @client = client
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end  
  end  

  def send_accept_contract client
    @client = client
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end  
  end

  def send_refuse_contract_document client
    @client = client
    
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end  
  end

  def send_accept_contract_document client
    @client = client
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end  
  end

  def send_signature_bank client
    @client = client
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end  
  end

  def send_refuse_bank client
    @client = client
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end
  end

  def send_accept_bank client
    @client = client
    unless @client.email.nil?
      mail(to: @client.email, subject: t('.subject'))
    end
  end
end
  