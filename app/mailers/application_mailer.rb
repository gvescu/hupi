class ApplicationMailer < ActionMailer::Base  
  default from: 'Hupi <contato@hupi.com.br>', content_type: "text/html"
  layout 'mailer'
end
