class UserMailer < ApplicationMailer
  def send_refuse_message(user_id)
    @user = User.find(user_id)
    mail(to: @user.email, subject: t('.subject'))
  end
end
