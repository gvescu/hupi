class ContractMailer < ApplicationMailer
  def deliver_contract(user_id)
    @user = User.find(user_id)
    file = @user.contract.file
    file_name = File.basename(file.path)

    attachments[file_name] = file.read
    mail(to: @user.email, subject: t('.subject'))
  end
end
