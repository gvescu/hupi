class PisFormatValidator < ActiveModel::EachValidator
  def validate_each(record, _attribute, value)
    record.errors.add(:pis, :invalid) unless PIS.valid?(value)
  end
end
