class EmailFormatValidator < ActiveModel::EachValidator
  REGEX = /\A[^@\s]+@[^@\s]+\z/

  def validate_each(record, attribute, value)
    return if valid?(value)
    record.errors.add(attribute, :invalid)
  end

  private

  def valid?(value)
    value.blank? || value =~ REGEX
  end
end
