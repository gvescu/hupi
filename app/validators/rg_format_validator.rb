class RgFormatValidator < ActiveModel::EachValidator
  RG_REGEX = /\A[0-9]+\z/

  def validate_each(record, _attribute, value)
    record.errors.add(:rg, :invalid) unless value.try(:match, RG_REGEX)
  end
end
