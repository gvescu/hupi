class CpfFormatValidator < ActiveModel::EachValidator
  def validate_each(record, _attribute, value)
    record.errors.add(:cpf, :invalid) unless CPF.valid?(value)
  end
end
