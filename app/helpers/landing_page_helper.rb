module LandingPageHelper
  def body_class
    content_for?(:body_class) ? content_for(:body_class) : 'pages'
  end
end
