module ApplicationHelper
  def page_title(separator = " - ")    
    return content_for?(:title) ? content_for(:title).concat("#{separator}#{app_name}") : app_name;
  end

  def app_name
    'HUPI'
  end
end
