class Document < ApplicationRecord      
  belongs_to :user, optional: true

  mount_uploader :file, DocumentUploader

  validates :file, presence: true  
  
end
