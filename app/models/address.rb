class Address < ApplicationRecord
  belongs_to :user, inverse_of: :address

  validates :number, presence: true
  validates :street, presence: true
  validates :zip_code, presence: true, format: { with: /\d{5}\-\d{3}/ }
  validates :city, presence: true
  validates :state, presence: true, inclusion: { in: Constants::STATES.keys }
end
