class User < ApplicationRecord
  STRICT_REGEX = /[.-]/

  STEPS = {
    bloc: 1,
    pavement: 2,
    apartment: 3,
    cpf_document: 4,
    rg_document: 5,
    residence_document: 6,
    employment_document: 7,
    analyze: 8,
    contract: 9,
    contractAnalyze: 10,
    boxAnalyze: 11,
    finished: 12
  }.freeze

  BLOCS = {
    a: "1",
    b: "2",
    c: "3",
    d: "4",    
  }.freeze

  PAVEMENTS = {
    a: "A",
    b: "B",
    c: "C",
    d: "D",    
    e: "E",
  }.freeze

  APARTMENTS = {
    a: "1",
    b: "2",
    c: "3",
    d: "4",  
    e: "5",        
    f: "6",        
    g: "7",        
    h: "8",        
  }.freeze

  STATUSES = {
    unreviewed: 0,
    refused: 1,
    accepted: 2
  }.freeze

  MARITAL_STATUSES = {
    single: 0,
    married: 1,
    divorced: 2,
    widowed: 3,
    separated: 4,
    companion: 5
  }.freeze

  enum current_step: STEPS, _suffix: :step
  enum status: STATUSES
  enum marital_status: MARITAL_STATUSES
  enum bloc: BLOCS, _suffix: :blocs
  enum pavement: PAVEMENTS, _suffix: :pavements
  enum apartment: APARTMENTS, _suffix: :apartments

  has_one :rg_document_front, dependent: :destroy
  has_one :rg_document_back, dependent: :destroy
  has_one :cpf_document, dependent: :destroy
  has_one :residence_document, dependent: :destroy
  has_one :employment_document, dependent: :destroy
  has_one :contract, dependent: :destroy
  has_one :contract_signature, dependent: :destroy
  has_many :documents
  has_one :address, inverse_of: :user

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :cpf_document 
  accepts_nested_attributes_for :rg_document_front
  accepts_nested_attributes_for :rg_document_back
  accepts_nested_attributes_for :residence_document
  accepts_nested_attributes_for :employment_document  
  accepts_nested_attributes_for :contract
  accepts_nested_attributes_for :contract_signature  

  validates :account_kit_id, presence: true, uniqueness: true
  validates :phone, presence: true
  validates :email, email_format: true, uniqueness: { scope: :phone }

  acts_as_paranoid
  devise :database_authenticatable, :trackable, :rememberable

  scope :not_admin, -> { where(admin: false) }
  scope :unreviewed_and_incomplete, -> { unreviewed.where.not(current_step: :finished) }
  scope :unreviewed_and_complete, -> { unreviewed.where(current_step: :finished) }  

  scope :accepted_contract, -> { where('users.contract_documents_approved is not null') }
  scope :refused_contract, -> { where('users.contract_documents_disapproved is not null') }  

  scope :incomplete, -> { where('users.received is null and approved is null') }
  scope :incomplete_with_document, -> { where('users.received is not null and approved is null') }  
  scope :complete_with_document, -> { where('users.approved is not null and signature_bank is null and contract_documents_disapproved is null') }  

  scope :finished_contract, -> { where('users.signature_bank is not null') }  
  scope :rejeited_contract, -> { where('users.contract_documents_disapproved is not null') }  
  

  def rg=(value)
    stripped_value = strip_document(value)
    super(stripped_value)
  end

  def cpf=(value)
    stripped_value = strip_document(value)
    super(stripped_value)
  end

  def pis=(value)
    stripped_value = strip_document(value)
    super(stripped_value)
  end

  def phone=(value)
    stripped_value = value.try(:gsub, /[() -]/, '')
    super(stripped_value)
  end

  def build_nested
    self.build_cpf_document if self.cpf_document.nil?    
    self.build_rg_document_front if self.rg_document_front.nil?
    self.build_rg_document_back if self.rg_document_back.nil?        
    self.build_residence_document if self.residence_document.nil?
    self.build_employment_document if self.employment_document.nil?    
    self.build_contract if self.contract.nil?    
    self.build_contract_signature if self.contract_signature.nil?        
    return self
  end

  def find_queue_from_apartment apartment_id
    User.where("bloc = ? and pavement = ? and apartment = ? and users.received is not null", User.blocs[self.bloc], User.pavements[self.pavement], User.apartments[apartment_id]).count
  end  

  private

  def strip_document(value)
    value.try(:gsub, STRICT_REGEX, '')
  end

  def status_change?
    self.status_changed? 
  end
end
