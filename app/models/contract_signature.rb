class ContractSignature < Document
    include CopyCarrierwaveFile  

    def duplicate_file_to_signature(user)
        copy_carrierwave_file(user.contract, self, :file)    
        self.user = user        
        self.save!
    end  
end
