var initMap = (function ($) {

  'use strict';

  var directionsSearched = "";
  var selectedDirection = undefined;

  var educationPoints = [    
  ];

  var mapPositionCenter = {
    lat: -25.30066, 
    lng: -57.63591,
    title: 'Asunción',
    address: ''
  };

  var markers = [];
  var panorama;
  var map;
  
  var TRAVEL_MODE = 'DRIVING';
  
  function initMap() {
    var mapEl = document.getElementById('education-map');

    if (mapEl !== null) {
      var directionsDisplay = new google.maps.DirectionsRenderer; 
      var directionsService = new google.maps.DirectionsService;
      
      // Create a map and center it on Criciúma/SC 
      map = new google.maps.Map(document.getElementById('education-map'), {
        zoom: 13,
        center: mapPositionCenter
      });
      directionsDisplay.setMap(map);
      directionsDisplay.setPanel(document.getElementById('js-bottom-panel'));
      
      handleAddMarkers();

      var $input = document.getElementById('directions');

      $input.addEventListener('blur', function(event) {
        // Searched terms. 
        directionsSearched = this.value;

        if (directionsSearched.length >= 0) {
          enableButtonTraceRoute();
        }
      });
    
      // Listen to submit event from the form. 
      var onSubmitHandler = function(event) {
        event.preventDefault();
    
        if (!emptyDirection()) {
          window.alert('Rellene su punto de partida y seleccione en el mapa el punto de educación para trazar una ruta.');
          this.focus();
        } else {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        }
      };
      document.getElementById('js-form-education-map').addEventListener('submit', onSubmitHandler);
  
      initializeStreetView();
      onClickTravelModeHandler();
      onClickTabsOptionsHandler();
    }
  }

  function enableButtonTraceRoute() {
    var $btn = document.getElementById('js-btn-directions');

    $btn.disabled = (emptyDirection() ? false : true);
  }

  function handleAddMarkers() {
    clearMarkers();
    for (var i = 0; i < educationPoints.length; i++) {
      addMarkerWithTimeout(educationPoints[i], i * 200, i);
    }
  }

  function addMarkerWithTimeout(position, timeout, index) {
    window.setTimeout(function() {
      var marker = new google.maps.Marker({
        position: position,
        map: map,
        animation: google.maps.Animation.DROP,
        title: position.title
      });

      markers.push(marker);
      
      attachAddressMessage(marker, position);
    }, timeout);
  }

  function attachAddressMessage(marker, position) {
    var content = 
      '<div>' +
        '<h1 class="firstHeading">' + position.title + '</h1>' +
        '<p>' + position.address + '</p>' +
      '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: content
    });

    marker.addListener('click', function() {
      infowindow.open(marker.get('map'), marker);

      setTimeout(function() {
        selectDirection(position);
      }, 2000);
    });
  }

  function selectDirection(position) {
    if (window.confirm('Usted desea seleccionar "' + position.title + '" como su punto de educación?')) {
      selectedDirection = position;
      
      document.getElementById('directions').focus();
    } else {
      selectedDirection = undefined;
    }

    enableButtonTraceRoute();
    return;
  }

  function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers = [];
  }
  
  function initializeStreetView() {
     panorama = new google.maps.StreetViewPanorama(
        document.getElementById('street-view'),
        {
          position: mapPositionCenter,
          pov: {heading: 165, pitch: 0},
          zoom: 1
        });
  }
  
  function emptyDirection () {
    return (directionsSearched == "" || typeof selectedDirection == "undefined") ? false : true;
  }
  
  function onClickTravelModeHandler() {
    var $travelmode = $('.js-directions-options li');
  
    $travelmode.on('click', function(event) {
      $travelmode.removeClass('active');
  
      $(this).addClass('active');
  
      TRAVEL_MODE = $(this).data('direction');
    });
  }
  
  function onClickTabsOptionsHandler() {
    var $tabsOptions = $('.component-tabs__options li');
  
    $('.component-tabs__options li').on('click', function(event) {
      event.preventDefault();
  
      if (!$(this).hasClass('active')) {
        $tabsOptions.removeClass('active');
        $(this).addClass('active');
    
        var tab = $(this).data('tab');
    
        toggleComponentBlocks(tab);
      }
    });
  }
  
  function toggleComponentBlocks(tab) {
    if (tab === 'street-view') {
      $('.component-tabs__block').hide();
    } else {
      $('.component-tabs__block').show();
    }
  
    $('.component-tabs .component-tabs__content:not(.js-' + tab + ')').css({
      zIndex: 1
    });
    $('.component-tabs .js-' + tab).css({
      zIndex: 2
    });
  }
  
  function calculateAndDisplayRoute(directionsService, directionsDisplay) {

    directionsService.route({
      origin: selectedDirection,
      destination: directionsSearched,
      travelMode: google.maps.TravelMode[TRAVEL_MODE],
      unitSystem: google.maps.UnitSystem.METRIC
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        throw new Error('Formato de dirección no válida');
        window.alert('Dirección no encontrada.\nIntente nuevamente informando su calle y ciudad.');
      }
    });
  }

  return initMap;

})(jQuery);