// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap
//= require jQuery-Mask-Plugin
// require education
//= require simulation

(function($) {
  'use strict';

  function isMobile()
  {
    var userAgent = navigator.userAgent.toLowerCase();
    
    if( userAgent.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 )
      return true;
  }

  function openCitiesModal() {
    $('#hupi-cities-modal').modal();    
  }
  
  //if (isMobile())
  //  $('#video-background').prop('muted', true);
    
  $(document).ready(function () {
      $('.carousel').carousel({
          interval: 5000
      });
  
      $('.carousel').carousel('cycle');
      
  });

  $(window).on('load', openCitiesModal);
})(jQuery);
