var initMap = (function ($) {

  'use strict';

  var directionsSearched = "";
  var selectedDirection = undefined;

  var educationPoints = [
    {
      lat: -28.7004202, 
      lng: -49.4118737,
      title: 'Terminal',
      address: 'Av. Universitária, 315 - Universitário'
    },
    {
      lat: -28.6858784, 
      lng: -49.4126626,
      title: 'Castagnetti',
      address: 'Rua Mafra, 178 - Boa vista'
    },
    {
      lat: -28.6904606, 
      lng: -49.3934947,
      title: 'Havan',
      address: 'Av. Centenário, 5074 - Santa Bárbara'
    },
    {
      lat: -28.669888, 
      lng: -49.4234263,
      title: 'Supermercados Manentti',
      address: 'Av. dos Imigrantes, 1698 - Rio Maina'
    },
    {
      lat: -28.687169, 
      lng: -49.4148122,
      title: 'Trevo',
      address: 'Av. dos Italiano, 2 - Rio Maina'
    },
    {
      lat: -28.6871641, 
      lng: -49.3355327,
      title: 'Nações Shopping',
      address: 'Av. Jorge Elias de Lucca, 879 - Na Sra da Salete'
    },
    {
      lat: -28.680377, 
      lng: -49.3470066,
      title: 'Fruteira',
      address: 'Rua General Osvaldo Pinto de Veiga, 1876 - Próspera'
    },
    {
      lat: -28.6802922, 
      lng: -49.3447553,
      title: 'Supermercados Manentti',
      address: 'Rua General Osvaldo Pinto de Veiga, 1340 - Próspera'
    }
  ];

  var mapPositionCenter = educationPoints[2];

  var markers = [];
  var panorama;
  var map;
  
  var TRAVEL_MODE = 'DRIVING';
  
  function initMap() {
    var mapEl = document.getElementById('education-map');

    if (mapEl !== null) {
      var directionsDisplay = new google.maps.DirectionsRenderer; 
      var directionsService = new google.maps.DirectionsService;
      
      // Create a map and center it on Criciúma/SC 
      map = new google.maps.Map(document.getElementById('education-map'), {
        zoom: 13,
        center: mapPositionCenter
      });
      directionsDisplay.setMap(map);
      directionsDisplay.setPanel(document.getElementById('js-bottom-panel'));
      
      handleAddMarkers();

      var $input = document.getElementById('directions');

      $input.addEventListener('blur', function(event) {
        // Searched terms. 
        directionsSearched = this.value;

        if (directionsSearched.length >= 0) {
          enableButtonTraceRoute();
        }
      });
    
      // Listen to submit event from the form. 
      var onSubmitHandler = function(event) {
        event.preventDefault();
    
        if (!emptyDirection()) {
          window.alert('Preencha seu ponto de partida e selecione no mapa o ponto de educação para traçar uma rota.');
          this.focus();
        } else {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        }
      };
      document.getElementById('js-form-education-map').addEventListener('submit', onSubmitHandler);
  
      initializeStreetView();
      onClickTravelModeHandler();
      onClickTabsOptionsHandler();
    }
  }

  function enableButtonTraceRoute() {
    var $btn = document.getElementById('js-btn-directions');

    $btn.disabled = (emptyDirection() ? false : true);
  }

  function handleAddMarkers() {
    clearMarkers();
    for (var i = 0; i < educationPoints.length; i++) {
      addMarkerWithTimeout(educationPoints[i], i * 200, i);
    }
  }

  function addMarkerWithTimeout(position, timeout, index) {
    window.setTimeout(function() {
      var marker = new google.maps.Marker({
        position: position,
        map: map,
        animation: google.maps.Animation.DROP,
        title: position.title
      });

      markers.push(marker);
      
      attachAddressMessage(marker, position);
    }, timeout);
  }

  function attachAddressMessage(marker, position) {
    var content = 
      '<div>' +
        '<h1 class="firstHeading">' + position.title + '</h1>' +
        '<p>' + position.address + '</p>' +
      '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: content
    });

    marker.addListener('click', function() {
      infowindow.open(marker.get('map'), marker);

      setTimeout(function() {
        selectDirection(position);
      }, 2000);
    });
  }

  function selectDirection(position) {
    if (window.confirm('Você deseja selecionar "' + position.title + '" como seu ponto de educação?')) {
      selectedDirection = position;
      
      document.getElementById('directions').focus();
    } else {
      selectedDirection = undefined;
    }

    enableButtonTraceRoute();
    return;
  }

  function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers = [];
  }
  
  function initializeStreetView() {
     panorama = new google.maps.StreetViewPanorama(
        document.getElementById('street-view'),
        {
          position: mapPositionCenter,
          pov: {heading: 165, pitch: 0},
          zoom: 1
        });
  }
  
  function emptyDirection () {
    return (directionsSearched == "" || typeof selectedDirection == "undefined") ? false : true;
  }
  
  function onClickTravelModeHandler() {
    var $travelmode = $('.js-directions-options li');
  
    $travelmode.on('click', function(event) {
      $travelmode.removeClass('active');
  
      $(this).addClass('active');
  
      TRAVEL_MODE = $(this).data('direction');
    });
  }
  
  function onClickTabsOptionsHandler() {
    var $tabsOptions = $('.component-tabs__options li');
  
    $('.component-tabs__options li').on('click', function(event) {
      event.preventDefault();
  
      if (!$(this).hasClass('active')) {
        $tabsOptions.removeClass('active');
        $(this).addClass('active');
    
        var tab = $(this).data('tab');
    
        toggleComponentBlocks(tab);
      }
    });
  }
  
  function toggleComponentBlocks(tab) {
    if (tab === 'street-view') {
      $('.component-tabs__block').hide();
    } else {
      $('.component-tabs__block').show();
    }
  
    $('.component-tabs .component-tabs__content:not(.js-' + tab + ')').css({
      zIndex: 1
    });
    $('.component-tabs .js-' + tab).css({
      zIndex: 2
    });
  }
  
  function calculateAndDisplayRoute(directionsService, directionsDisplay) {

    directionsService.route({
      origin: selectedDirection,
      destination: directionsSearched,
      travelMode: google.maps.TravelMode[TRAVEL_MODE],
      unitSystem: google.maps.UnitSystem.METRIC
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        throw new Error('Formato de endereço inválido.');
        window.alert('Endereço não encontrado.\nTente novamente informando sua rua e cidade.');
      }
    });
  }

  return initMap;

})(jQuery);