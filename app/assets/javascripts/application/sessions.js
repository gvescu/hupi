(function($) {
  'use strict';

  var PHONE_MASK = '(00) 00000-0000';

  function redirectUser() {
    window.location = '/usuario/editar';
  }

  function showError() {
    $('.js-alert').show();
    $('.js-login-form input').prop('disabled', false);
  }

  function loginCallback(response) {
    if (response.status === 'PARTIALLY_AUTHENTICATED') {
      $.ajax({
        url: '/usuarios/entrar',
        method: 'POST',
        data: { code: response.code, state: response.state, email: $("#email_").val() }
      }).done(redirectUser)
        .fail(showError);
    } else {
      showError();
    }
  }

  function smsLogin(e) {
    var phone = $('.js-phone-field').val();

    e.preventDefault();
    $('.js-alert').hide();
    AccountKit.login('PHONE', { countryCode: '+55', phoneNumber: phone }, loginCallback);
  }

  function initLoginForm() {
    $('.js-phone-field').mask(PHONE_MASK);
    $('.js-login-form').on('submit', smsLogin);
  }

  $(initLoginForm);
})(jQuery);
