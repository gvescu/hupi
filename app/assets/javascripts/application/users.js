(function($) {
  'use strict';

  var CPF_MASK = '000.000.000-00';
  var PIS_MASK = '000.00000.00-0';
  var ZIP_CODE_MASK = '00000-000';

  function initMasks() {
    $('.js-cpf-field').mask(CPF_MASK, { placeholder: CPF_MASK });
    $('.js-pis-field').mask(PIS_MASK, { placeholder: PIS_MASK });
    $('.js-zip-code-field').mask(ZIP_CODE_MASK, { placeholder: ZIP_CODE_MASK });
  }

  function initApproveButton() {
    $('.js-approve-button').on('click', function(e) {
      $('.js-contract-form input').click();
    });
  }

  function initContractField() {
    $('.js-contract-form input').on('change', function(e) {
      $('.js-contract-form').submit();
    });
  }

  $(function() {
    initMasks();
    initApproveButton();
    initContractField();
  });
})(jQuery);
