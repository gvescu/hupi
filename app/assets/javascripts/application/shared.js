(function($) {
  'use strict';

  function initFileInputFields() {
    $('input[type="file"]').on('change', function(e) {
      var size = e.target.files[0].size;

      if (size > 10485760) {
        alert('Arquivo muito grande!');
        this.value = '';
        return;
      }
    });
  }

  $(initFileInputFields);
})(jQuery);
