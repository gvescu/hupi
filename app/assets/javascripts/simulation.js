(function($) {
    'use strict';
  
    var PT_BR_MASK = '#.##0,00';
    var PY_MASK = '#.##0';
  
    function initMask() {
      $('.js-money-pt-BR').mask(PT_BR_MASK, { reverse: true });
      $('.js-money-py').mask(PY_MASK, { reverse: true });
    }
  
    $(initMask);
  })(jQuery);
  