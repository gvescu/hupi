$(document).ready(function(){    

    $(".file").change(function(event){
       var input = $(event.currentTarget);       
       var file = input[0].files[0];
       var reader = new FileReader();
       reader.onload = function(e){
           image_base64 = e.target.result;
           var type = input.data('type');
           if (type && type == 'pdf') {
            $("." + input.data('document') + "-upload-preview").removeClass('hidden');
            $("." + input.data('document') + "-button").addClass('hidden');
            $("." + input.data('document') + "-upload").removeClass('hidden');            
           }            
           else                      
            $("." + input.data('document') + "-upload-preview img").attr("src", image_base64);
       };
       reader.readAsDataURL(file);
    });
    $("input:radio[name='user[apartment]']").change(function(event) {
        $.get("/apartamentos/" + $(event.target).val() + "/queue");
    });
    
});