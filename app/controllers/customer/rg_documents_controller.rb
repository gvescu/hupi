module Customer
  class RgDocumentsController < ApplicationController
    before_action -> { check_current_step(step: :rg_document) }

    def edit
      @document = current_user.build_rg_document
    end

    def update
      @document = current_user.build_rg_document(document_params)

      if @document.save && current_user.update(current_step: :residence_document)
        redirect_to(edit_residence_document_path)
      else
        flash.now[:alert] = t('application.rg_documents.update.alert')
        render(:edit)
      end
    end

    private

    def document_params
      params.require(:rg_document).permit(:file)
    end
  end
end
