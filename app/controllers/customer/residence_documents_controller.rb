module Customer
  class ResidenceDocumentsController < ApplicationController
    before_action -> { check_current_step(step: :residence_document) }

    def edit
      @document = current_user.build_residence_document
    end

    def update
      @document = current_user.build_residence_document(document_params)

      if @document.save && current_user.update(current_step: :employment_document)
        redirect_to(edit_employment_document_path)
      else
        flash.now[:alert] = t('application.residence_documents.update.alert')
        render(:edit)
      end
    end

    private

    def document_params
      params.require(:residence_document).permit(:file)
    end
  end
end
