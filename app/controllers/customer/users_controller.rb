module Customer
  class UsersController < ApplicationController
    layout 'application'

    before_action -> { check_current_step(step: :data) }    

    PERMITTED_PARAMS = [
      :name, :email, :rg, :cpf, :pis, :birthdate, :marital_status, :family_income,
      address_attributes: %i(id number street zip_code complement neighborhood city state)
    ].freeze

    def edit      
      @user = current_user
      @user.build_address(city: 'Criciúma', state: 'SC')      
    end

    def update
      @user = current_user

      if @user.update(user_params)
        redirect_to(edit_cpf_document_path)
      else
        render(:edit)
      end
    end

    private

    def user_params
      params
        .require(:user)
        .permit(PERMITTED_PARAMS)
        .merge(current_step: :rg_document)
    end
  end
end
