module Customer
  class ContractsController < ApplicationController
    before_action -> { check_current_step(step: :contract) }

    def show; end
  end
end
