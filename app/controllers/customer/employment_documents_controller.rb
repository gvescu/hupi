module Customer
  class EmploymentDocumentsController < ApplicationController
    before_action -> { check_current_step(step: :employment_document) }

    def edit
      @document = current_user.build_employment_document
    end

    def update
      @document = current_user.build_employment_document(document_params)

      if @document.save && current_user.update(current_step: :contract)
        redirect_to(contract_path)
      else
        flash.now[:alert] = t('application.employment_documents.update.alert')
        render(:edit)
      end
    end

    private

    def document_params
      params.require(:employment_document).permit(:file)
    end
  end
end
