module Shop
  class BlocsController < ApplicationController
    layout 'application'

    #before_action -> { check_current_step(step: :bloc) }

    PERMITTED_PARAMS = [
      :bloc
    ].freeze

    def edit
      @user = current_user      
    end

    def update
      @user = current_user

      if @user.update(user_params)
        redirect_to(edit_pavement_pt_br_path)
      else
        render(:edit)
      end
    end

    private

    def user_params
      if User.current_steps[current_user.current_step].to_i > User.current_steps[:pavement]
        params
        .require(:user)
        .permit(PERMITTED_PARAMS)
      else  
        params
          .require(:user)
          .permit(PERMITTED_PARAMS)
          .merge(current_step: :pavement)
      end    
    end
  end
end
