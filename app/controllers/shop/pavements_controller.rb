module Shop
  class PavementsController < ApplicationController
    layout 'application'

    #before_action -> { check_current_step(step: :pavement) }

    PERMITTED_PARAMS = [
      :pavement
    ].freeze

    def edit
      @user = current_user      
    end

    def update
      @user = current_user

      if @user.update(user_params)
        redirect_to(edit_apartment_path)
      else
        render(:edit)
      end
    end

    private

    def user_params
      if User.current_steps[current_user.current_step].to_i > User.current_steps[:apartment]
        params
        .require(:user)
        .permit(PERMITTED_PARAMS)
      else  
        params
          .require(:user)
          .permit(PERMITTED_PARAMS)
          .merge(current_step: :apartment)
      end         
    end
  end
end
