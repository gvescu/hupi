module Shop
  class DocumentsController < ApplicationController
    layout 'application'

    def edit
      @user = current_user.build_nested            
    end

    def update      
      if current_user.update(user_params)      
        redirect_to(analyze_pt_br_path)
      else
        render(:edit)
      end
    end

    private

    def user_params
      if User.current_steps[current_user.current_step].to_i >= User.current_steps[:analyze]
        if current_user.received.nil?
          params.require(:user).permit(
            :disapproved,
            cpf_document_attributes: [:id, :file, :file_cache],
            rg_document_front_attributes: [:id, :file, :file_cache],
            rg_document_back_attributes: [:id, :file, :file_cache],
            employment_document_attributes: [:id, :file, :file_cache],
            residence_document_attributes: [:id, :file, :file_cache]
          ).merge(received: Time.now)
        else  
          params.require(:user).permit(
            :disapproved,
            cpf_document_attributes: [:id, :file, :file_cache],
            rg_document_front_attributes: [:id, :file, :file_cache],
            rg_document_back_attributes: [:id, :file, :file_cache],
            employment_document_attributes: [:id, :file, :file_cache],
            residence_document_attributes: [:id, :file, :file_cache]
          )
        end  
      else  
        params.require(:user).permit(
          :disapproved,
          cpf_document_attributes: [:id, :file, :file_cache],
          rg_document_front_attributes: [:id, :file, :file_cache],
          rg_document_back_attributes: [:id, :file, :file_cache],
          employment_document_attributes: [:id, :file, :file_cache],
          residence_document_attributes: [:id, :file, :file_cache]
        ).merge(current_step: :analyze, received: Time.now)
      end        
    end
  end
end
