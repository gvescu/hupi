module Shop
  class ContractsController < ApplicationController
    layout 'application'

    def edit
      #@url_document = ClickSign.get_document_url current_user.document_key if current_user.document_key
      @user = current_user.build_nested                   
    end

    def update      

      if current_user.contract_signature.nil?
        current_user.signature_contract_user_agent = request.user_agent
        current_user.signature_contract_remote_ip = request.remote_ip      
        current_user.signature_contract_hash = OpenSSL::HMAC.hexdigest('sha256', ENV['HUPI_HMAC'], current_user.signature_contract_user_agent + current_user.signature_contract_remote_ip)          
        current_user.signature_contract_time = Time.now
      
        document = ContractSignature.new
        document.duplicate_file_to_signature(current_user)
      end  
      
      if current_user.update(user_params)      
        redirect_to(analyze_contract_pt_br_path)
      else
        render(:edit)
      end
    end

    def callback
      callback
    end  

    private

    def user_params
      if User.current_steps[current_user.current_step].to_i > User.current_steps[:contractAnalyze]
        params.require(:user).permit(        
          :contract_refused, :analyze_contract, :signature,
          :signature_contract_accepted,
          contract_signature_attributes: [:id, :file, :file_cache]        
        )
      else  
        params.require(:user).permit(        
          :contract_refused, :analyze_contract, :signature,
          :signature_contract_accepted,
          contract_signature_attributes: [:id, :file, :file_cache]
        ).merge(current_step: :contractAnalyze)
      end  
    end
  end
end
