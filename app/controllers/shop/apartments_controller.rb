module Shop
  class ApartmentsController < ApplicationController
    layout 'application'

    #before_action -> { check_current_step(step: :pavement) }

    PERMITTED_PARAMS = [
      :apartment
    ].freeze

    def edit
      @user = current_user      
    end

    def update
      @user = current_user

      if @user.update(user_params)
        redirect_to(edit_document_path)
      else
        render(:edit)
      end
    end

    def queue
      @queue_number = current_user.find_queue_from_apartment params[:apartment_id]      
    end  

    private

    def user_params
      if User.current_steps[current_user.current_step].to_i > User.current_steps[:cpf_document]
        params
        .require(:user)
        .permit(PERMITTED_PARAMS)
      else  
        params
          .require(:user)
          .permit(PERMITTED_PARAMS)
          .merge(current_step: :cpf_document)
      end          
    end
  end
end
