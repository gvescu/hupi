module Shop
  class AnalyzeBanksController < ApplicationController
    layout 'application'

    def show
      @user = current_user.build_nested                  
    end    
  end
end
