module Admin
  class ContractsController < BaseController
    def create
      create_and_send_contract_via_email
      redirect_to(admin_users_path, notice: t('.notice'))
    end

    private

    def create_and_send_contract_via_email
      user = User.find(params[:user_id])
      contract = user.build_contract(file: params[:file])

      ActiveRecord::Base.transaction do
        contract.save!
        user.update!(status: :accepted)
        ContractMailer.deliver_contract(user.id).deliver_later
      end
    end
  end
end
