module Admin
  class UsersController < BaseController
    before_action :set_user, except: [:index]
    before_action :upload_contract, only: [:update]
    layout 'application'

    PERMITTED_PARAMS = [
      :id, :name, :email, :rg, :cpf, :pis, :birthdate, :marital_status, :family_income, :phone,
      :analyze, :approved, :disapproved, :contact, :signature, :current_step, :elaborate_contract,
      :contract_documents_approved, :contract_documents_disapproved, :signature_bank,
      :contract_approved, :contract_refused, :disapproved_bank, :approved_bank, :analyze_contract,            
      contract_attributes: [:id, :file, :file_cache, :load_contract],
      address_attributes: %i(id number street zip_code complement neighborhood city state)
    ].freeze

    def index
      @incomplete = User.not_admin.incomplete.order(:name)
      @incomplete_with_document = User.not_admin.incomplete_with_document.order(:name)
      @complete_with_document = User.not_admin.complete_with_document.order(:name)

      @finished_contract = User.not_admin.finished_contract.order(:name)
      @rejeited_contract = User.not_admin.rejeited_contract.order(:name)
    end

    def show      
      @user = @user.build_nested
    end

    def edit      
      @user.build_address(city: 'Criciúma', state: 'SC') if @user.address.blank?
    end

    def update          
      if @user.update(user_params)                   
        redirect_to(admin_user_path(@user))
      else
        render(:edit)
      end
    end    

    def approved      
      ActiveRecord::Base.transaction do
        if @user.update!(approved: Time.now, disapproved: nil)
          ShopMailer.send_accept_document(@user).deliver        
          redirect_to(admin_user_path(@user))
        else  
          render(:edit)
        end        
      end            
    end  

    def approved_contract    
      ActiveRecord::Base.transaction do
        if @user.update!(current_step: :boxAnalyze, contract_approved: Time.now)
          ShopMailer.send_accept_contract(@user).deliver        
          redirect_to(admin_user_path(@user))
        else  
          render(:edit)
        end        
      end            
    end  

    def approved_contract_document
      ActiveRecord::Base.transaction do
        if @user.update!(contract_documents_approved: Time.now, contract_documents_disapproved: nil)
          ShopMailer.send_accept_contract_document(@user).deliver        
          redirect_to(admin_user_path(@user))
        else  
          render(:edit)
        end        
      end            
    end  

    def approved_contract_document
      ActiveRecord::Base.transaction do
        if @user.update!(contract_documents_approved: Time.now, contract_documents_disapproved: nil)
          ShopMailer.send_accept_contract_document(@user).deliver        
          redirect_to(admin_user_path(@user))
        else  
          render(:edit)
        end        
      end            
    end  

    def signature_bank      
      if @user.update(user_params)           
        ShopMailer.send_signature_bank(@user).deliver                
        redirect_to(admin_user_path(@user))
      else
        render(:edit)
      end
    end

    def refuse
      refuse_user_and_notify_via_email
      redirect_to admin_users_path, notice: t('.notice')
    end

    def refuse_contract
      refuse_contract_and_notify_via_email      
      redirect_to admin_users_path, notice: t('.notice')
    end

    def refuse_contract_document
      refuse_contract_document_and_notify_via_email
      redirect_to admin_users_path, notice: t('.notice')
    end

    def refuse_bank
      refuse_bank_and_notify_via_email
      redirect_to admin_users_path, notice: t('.notice')
    end

    def destroy
      User.find(params[:id]).destroy
      redirect_to admin_users_path, notice: t('.notice')
    end

    def reset_account
      user = User.find(params[:id])

      if user.update(
          :current_step => 1,
          :bloc => nil,
          :pavement => nil,
          :apartment => nil,
          :received => nil,
          :analyze => nil,
          :approved => nil,
          :disapproved => nil,
          :contact => nil,
          :signature => nil,
          :approved_bank => nil,
          :contract_refused => nil,
          :contract_approved => nil,
          :disapproved_bank => nil,
          :analyze_contract => nil,
          :elaborate_contract => nil,
          :contract_documents_disapproved => nil,
          :contract_documents_approved => nil,
          :signature_bank => nil,
          :rg_document_front => nil,
          :rg_document_back => nil,
          :cpf_document => nil,
          :residence_document => nil,
          :employment_document => nil,
          :contract => nil,
          :contract_signature => nil
        )
        redirect_to admin_users_path, notice: t('.notice_reset')
      else
        render(:edit)        
      end    
            
    end  

    private

    def upload_contract
      if !user_params[:contract_attributes].nil? && the_file = user_params[:contract_attributes][:file]
        the_file = user_params[:contract_attributes][:file] 

        # comentado integração com clicksign
        #File.open(the_file.tempfile, 'r') do |file|                
        #  document = ClickSign.create_document(File.new(file), the_file.original_filename, @user)
          #unless document && @user.update(document_key: document['document']['key'])
          #  redirect_to admin_user_path(@user), notice: t('.problem_contract')
          #end                  
        #end  
      end  
    end

    def refuse_user_and_notify_via_email
      ActiveRecord::Base.transaction do
        @user.update!(approved: nil, cpf_document: nil, employment_document: nil, residence_document: nil ,rg_document_back: nil, rg_document_front: nil, received: nil, disapproved: Time.now, contract_refused: nil, current_step: 4)
        ShopMailer.send_refuse_document(@user).deliver        
      end
    end

    def refuse_contract_and_notify_via_email
      ActiveRecord::Base.transaction do
        @user.update!(contract: nil, contact: nil, analyze_contract: nil, signature: nil, contract_approved: nil, contract_signature: nil, contract_refused: Time.now)
        ClickSign.cancel(@user.key) if @user.key
        ShopMailer.send_refuse_contract(@user).deliver        
      end
    end    

    def refuse_contract_document_and_notify_via_email
      ActiveRecord::Base.transaction do
        #@user.update!(contract_documents_disapproved: Time.now)     

        ShopMailer.send_refuse_contract_document(@user).deliver           
      end
    end    

    def refuse_bank_and_notify_via_email
      ActiveRecord::Base.transaction do
        @user.update!(approved_bank: nil, elaborate_contract: nil, disapproved_bank: Time.now, current_step: 10)
        ShopMailer.send_refuse_bank(@user).deliver           
      end
    end    

    def set_user
      @user = User.find(params[:id])
    end  

    def user_params
      params.require(:user).permit(PERMITTED_PARAMS)
    end
  end
end
