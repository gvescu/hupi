module Admin
  class ResidenceDocumentsController < BaseController
    def edit
      @document = user.residence_document
    end

    def update
      @document = user.residence_document

      if @document.update(file: params[:residence_document][:file])
        redirect_to(admin_user_path(user))
      else
        flash.now[:alert] = t('application.residence_documents.update.alert')
        render(:edit)
      end
    end

    private

    def user
      @user ||= User.find(params[:user_id])
    end
  end
end
