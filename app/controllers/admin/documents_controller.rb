module Admin
  class DocumentsController < ApplicationController
    layout 'application'

    def edit
      @user = user.build_nested            
    end

    def update      
      if user.update(user_params)      
        redirect_to(admin_user_path(user))
      else
        render(:edit)
      end
    end

    private

    def user_params
      params.require(:user).permit(        
        cpf_document_attributes: [:id, :file, :file_cache],
        rg_document_front_attributes: [:id, :file, :file_cache],
        rg_document_back_attributes: [:id, :file, :file_cache],
        employment_document_attributes: [:id, :file, :file_cache],
        residence_document_attributes: [:id, :file, :file_cache],
        contract_attributes: [:id, :file, :file_cache],
        contract_signature_attributes: [:id, :file, :file_cache]        
      ).merge(current_step: :analyze, received: Time.now)
    end

    def user
      @user ||= User.find(params[:user_id])
    end
  end
end
