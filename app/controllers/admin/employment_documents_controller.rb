module Admin
  class EmploymentDocumentsController < BaseController
    def edit
      @document = user.employment_document
    end

    def update
      @document = user.employment_document

      if @document.update(file: params[:employment_document][:file])
        redirect_to(admin_user_path(user))
      else
        flash.now[:alert] = t('application.employment_documents.update.alert')
        render(:edit)
      end
    end

    private

    def user
      @user ||= User.find(params[:user_id])
    end
  end
end
