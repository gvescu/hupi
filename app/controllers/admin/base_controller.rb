module Admin
  class BaseController < ApplicationController
    before_action :check_permissions

    def check_permissions
      return head(401) unless current_user.admin?
    end
  end
end
