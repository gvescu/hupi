module Admin
  class RgDocumentsController < BaseController
    def edit
      @document = user.rg_document
    end

    def update
      @document = user.rg_document

      if @document.update(file: params[:rg_document][:file])
        redirect_to(admin_user_path(user))
      else
        flash.now[:alert] = t('application.rg_documents.update.alert')
        render(:edit)
      end
    end

    private

    def user
      @user ||= User.find(params[:user_id])
    end
  end
end
