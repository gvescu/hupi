class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale
  before_action :set_raven_context

  helper_method :is_py?, :is_br?

  HOSTS_MAPPING = {
    'hupi.com.br'     => 'pt-BR',
    'hupi.com.py'     => 'py',
    'www.hupi.com.br' => 'pt-BR',
    'www.hupi.com.py' => 'py'    
  }

  STEP_ENDPOINTS = {
    bloc: { controller: 'shop/blocs', action: :edit },
    pavement: { controller: 'shop/pavements', action: :edit },
    apartment: { controller: 'shop/apartments', action: :edit },
    document: { controller: 'shop/documents', action: :edit },
    cpf_document: { controller: 'shop/documents', action: :edit },
    rg_document_front: { controller: 'shop/documents', action: :edit },
    rg_document_back: { controller: 'shop/documents', action: :edit },
    residence_document: { controller: 'shop/documents', action: :edit },
    employment_document: { controller: 'shop/documents', action: :edit },
    analyze: { controller: 'shop/analyzes', action: :show },
    contract: { controller: 'shop/contracts', action: :edit },
    contractAnalyze: { controller: 'shop/analyze_contracts', action: :show },
    boxAnalyze: { controller: 'shop/analyze_banks', action: :show },
    finished: { controller: 'shop/analyze_banks', action: :show }        
  }.freeze  

  def check_current_step(step:)        
    return if current_user.current_step.to_sym == step
    redirect_to(current_step_url)
  end

  def current_step_url(user = current_user)    
    endpoint = STEP_ENDPOINTS[user.current_step.to_sym]    
    url_for(endpoint)
  end

  def after_sign_in_path_for(resource)    
    resource.admin? ? admin_users_path : current_step_url(resource)
  end

  def is_py?
    I18n.locale == :'py'
  end

  def is_br?
    I18n.locale == :'pt-BR'
  end

  private

  def set_raven_context
    Raven.user_context(id: session[:current_user_id])
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

  def set_locale        
    if Rails.env.development?
      I18n.locale = :'pt-BR'
    else  
      I18n.locale = HOSTS_MAPPING[request.host] || I18n.default_locale      
    end      
  end
end
