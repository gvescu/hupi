class SessionsController < ApplicationController
  layout 'users'

  def new
    session[:state] = form_authenticity_token          
  end

  def create
    return head(:bad_request) if params[:state] != session[:state]

    user = find_user_and_update_phone(params[:code])  
    
    user.email = params[:email] if !params[:email].blank?
    if user.save      
      sign_in_and_redirect(user)           
    else
      head(:bad_request)
    end
  end

  def destroy
    sign_out_and_redirect(current_user)
  end

  private

  def find_user_and_update_phone(code)
    exchanger = Facebook::AccountKit::TokenExchanger.new(code)
    token = exchanger.fetch_access_token
    data = Facebook::AccountKit::UserAccount.new(token).fetch_user_info

    user = User.find_or_initialize_by(account_kit_id: data['id'])
    user.phone = data['phone']['national_number']        
    user
  end
end
