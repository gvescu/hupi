class DocumentDownloadsController < ApplicationController
  def show
    if Rails.env.development?
      send_file(document.file.path, disposition: :inline)
    else  
      data = open(document.file.url)
      send_data data.read, type: data.content_type, x_sendfile: true
    end
  end

  def donwload_contract    
    if Rails.env.development?
      send_file(document.file.path,
        :filename => "contrato.pdf",
        :type => "application/pdf")
    else
      data = open(document.file.url)
      send_data data.read, type: data.content_type, x_sendfile: true
    end      
  end 
  
  def donwload_video       
    if I18n.locale == :'pt-BR'
      send_file("#{Rails.root}/app/assets/videos/home-video-download.mp4",      
          :filename => "video-hupi.mp4",
          :type => "video/mp4")                
    else       
      send_file("#{Rails.root}/app/assets/videos/home-video-download-py.mp4",      
        :filename => "video-hupi-py.mp4",
        :type => "video/mp4")          
    end  
  end 

  private

  def document
    current_user.admin? ? Document.find(params[:id]) : current_user.documents.find(params[:id])
  end
end
