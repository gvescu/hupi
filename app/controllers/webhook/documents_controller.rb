class Webhook::DocumentsController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :validate

  def callback   
    render :nothing => true, :status => 200
  end  

  private

  def validate
    ClickSign.validate_callback params[:event][:name], params[:document][:key], request
  end  
end