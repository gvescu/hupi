class LandingPageController < ApplicationController
  layout 'home'

  def home; end

  def privacy; end

  def purpose; end

  def differential; end

  def education_points; end

  def terms_of_service; end

  def simulation; end

  def simulator
    if is_br?
      @simulation = Simulation.calculate_br params[:simulator][:lace]
    else
      @simulation = Simulation.calculate_py params[:simulator][:lace]
    end        

    render :simulation
  end

  def future_releases; end

  def tour; end

  def sustainability; end
end
