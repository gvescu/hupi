require 'test_helper'

class ContractMailerTest < ActionMailer::TestCase
  test '#deliver_contract sends contract to user' do
    user = create(
      :user_with_address,
      name: 'Ford Prefect',
      email: 'ford@magrathea.com',
      current_step: :contract,
      contract: build(:contract),
      status: :accepted
    )
    email = ContractMailer.deliver_contract(user.id)

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal ['contato@hupi.com.br'], email.from
    assert_equal ['ford@magrathea.com'], email.to
    assert_equal I18n.t('contract_mailer.deliver_contract.subject'), email.subject
    assert_match 'Ford Prefect', email.html_part.to_s
    assert_match 'Seu cadastro foi aprovado', email.html_part.to_s
    assert_match 'Ford Prefect', email.text_part.to_s
    assert_match 'Seu cadastro foi aprovado', email.text_part.to_s
    assert_equal 1, email.attachments.size
  end
end
