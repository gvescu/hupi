require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test '#send_refuse_message sends refuse message to user' do
    user = create(
      :user_with_address,
      name: 'Ford Prefect',
      email: 'ford@magrathea.com',
      current_step: :contract,
      status: :refused
    )
    email = UserMailer.send_refuse_message(user.id)

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal ['contato@hupi.com.br'], email.from
    assert_equal ['ford@magrathea.com'], email.to
    assert_equal I18n.t('user_mailer.send_refuse_message.subject'), email.subject
    assert_match 'Ford Prefect', email.html_part.to_s
    assert_match 'Encontramos uma falha no seu cadastro', email.html_part.to_s
    assert_match 'Ford Prefect', email.text_part.to_s
    assert_match 'Encontramos uma falha no seu cadastro', email.text_part.to_s
  end
end
