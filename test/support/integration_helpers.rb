module IntegrationHelpers
  def stub_account_kit(account_kit_id, phone)
    stub_request(:get, %r{//graph.accountkit.com/v1.1/access_token})
      .to_return(body: { access_token: 'token' }.to_json)
    stub_request(:get, %r{//graph.accountkit.com/v1.1/me})
      .to_return(body: { id: account_kit_id, phone: { national_number: phone } }.to_json)
  end
end
