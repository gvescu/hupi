module PageComponents
  class Navbar < PageObject
    def shown?
      @page.has_content?(t('title')) && @page.has_link?(t('navbar.sign_out'))
    end

    def click_on_log_out
      @page.click_on(t('navbar.sign_out'))
    end
  end
end
