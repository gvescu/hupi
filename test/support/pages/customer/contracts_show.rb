module Pages
  module Customer
    class ContractsShow < PageObject
      def initialize(page, user)
        @user = user
        super(page)
      end

      def visit
        @page.visit('/contrato')
      end

      def shown?
        @page.has_content?(t('customer.contracts.show.title'))
      end

      def has_unreviewed_message?
        header = t('customer.contracts.unreviewed_message.header')
        description = t('customer.contracts.unreviewed_message.description_html')

        @page.has_content?(header) && @page.body.include?(description)
      end

      def has_refused_message?
        header = t('customer.contracts.refused_message.header')
        description = t('customer.contracts.refused_message.description', name: @user.name)

        @page.has_content?(header) && @page.has_content?(description)
      end

      def has_accepted_message?
        header = t('customer.contracts.accepted_message.header')
        description = t('customer.contracts.accepted_message.description_html', email: @user.email)

        @page.has_content?(header) && @page.body.include?(description)
      end

      def contract_downloaded?
        @page.response_headers['Content-Disposition'].match('inline')
      end
    end
  end
end
