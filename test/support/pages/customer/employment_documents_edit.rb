module Pages
  module Customer
    class EmploymentDocumentsEdit < PageObject
      def visit
        @page.visit('/carteira_de_trabalho/editar')
      end

      def shown?
        @page.has_content?(t('application.employment_documents.edit.title'))
      end

      def has_errors?
        @page.has_content?(t('application.employment_documents.update.alert'))
      end

      def select_file(file)
        @page.attach_file('employment_document[file]', file)
      end

      def click_submit
        @page.click_on(t('application.employment_documents.edit.submit'))
      end

      def has_file?(file)
        @page.has_content?(t('application.employment_documents.edit.uploaded_file', file: file))
      end
    end
  end
end
