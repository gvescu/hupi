module Pages
  module Customer
    class SessionsNew < PageObject
      def visit
        @page.visit('/usuarios/entrar')
      end

      def shown?
        @page.has_content?(/#{t('sessions.new.sign_in')}/i)
      end

      def error_message_is_shown?
        @page.has_content?(t('sessions.new.alert_html'))
      end

      def fill_in_phone(phone)
        @page.fill_in('phone', with: phone)
      end

      def click_on_log_in
        @page.click_on(t('sessions.new.sign_in'))
      end
    end
  end
end
