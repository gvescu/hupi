module Pages
  module Customer
    class RgDocumentsEdit < PageObject
      def visit
        @page.visit('/rg/editar')
      end

      def shown?
        @page.has_content?(t('application.rg_documents.edit.title'))
      end

      def has_errors?
        @page.has_content?(t('application.rg_documents.update.alert'))
      end

      def select_file(file)
        @page.attach_file('rg_document[file]', file)
      end

      def click_submit
        @page.click_on(t('application.rg_documents.edit.submit'))
      end

      def has_file?(file)
        @page.has_content?(t('application.rg_documents.edit.uploaded_file', file: file))
      end
    end
  end
end
