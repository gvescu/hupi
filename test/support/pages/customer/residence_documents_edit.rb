module Pages
  module Customer
    class ResidenceDocumentsEdit < PageObject
      def visit
        @page.visit('/comprovante_de_residencia/editar')
      end

      def shown?
        @page.has_content?(t('application.residence_documents.edit.title'))
      end

      def has_errors?
        @page.has_content?(t('application.residence_documents.update.alert'))
      end

      def select_file(file)
        @page.attach_file('residence_document[file]', file)
      end

      def click_submit
        @page.click_on(t('application.residence_documents.edit.submit'))
      end

      def has_file?(file)
        @page.has_content?(t('application.residence_documents.edit.uploaded_file', file: file))
      end
    end
  end
end
