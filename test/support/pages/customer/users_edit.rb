module Pages
  module Customer
    class UsersEdit < PageObject
      attr_reader :navbar

      delegate :click_on_log_out, to: :navbar

      def initialize(page)
        @navbar = PageComponents::Navbar.new(page)
        super
      end

      def visit
        @page.visit('/usuario/editar')
      end

      def shown?
        @page.has_content?(t('application.users.edit.title')) &&
          @page.has_button?(t('application.users.edit.submit'))
      end

      def has_errors?(count:)
        error_message = t('activerecord.errors.messages.not_saved', count: count)
        @page.has_content?(error_message)
      end

      def has_blank_error?
        @page.has_content?(t('errors.messages.blank'))
      end

      def fill_name(name)
        @page.fill_in(User.human_attribute_name(:name), with: name)
      end

      def fill_email(email)
        @page.fill_in(User.human_attribute_name(:email), with: email)
      end

      def fill_rg(rg)
        @page.fill_in(User.human_attribute_name(:rg), with: rg)
      end

      def fill_cpf(cpf)
        @page.fill_in(User.human_attribute_name(:cpf), with: cpf)
      end

      def fill_pis(pis)
        @page.fill_in(User.human_attribute_name(:pis), with: pis)
      end

      def fill_birthdate(day, month, year)
        @page.select(day, from: 'user[birthdate(3i)]')
        @page.select(month, from: 'user[birthdate(2i)]')
        @page.select(year, from: 'user[birthdate(1i)]')
      end

      def select_marital_status(marital_status)
        marital_status_text = User.human_enum_name(:marital_status, marital_status)
        @page.select(marital_status_text, from: User.human_attribute_name(:marital_status))
      end

      def fill_family_income(family_income)
        @page.fill_in(User.human_attribute_name(:family_income), with: family_income)
      end

      def fill_street(street)
        @page.fill_in(Address.human_attribute_name(:street), with: street)
      end

      def fill_number(number)
        @page.fill_in(Address.human_attribute_name(:number), with: number)
      end

      def fill_zip_code(zip_code)
        @page.fill_in(Address.human_attribute_name(:zip_code), with: zip_code)
      end

      def fill_complement(complement)
        @page.fill_in(Address.human_attribute_name(:complement), with: complement)
      end

      def fill_neighborhood(neighborhood)
        @page.fill_in(Address.human_attribute_name(:neighborhood), with: neighborhood)
      end

      def click_on_save
        @page.click_on(t('application.users.edit.submit'))
      end
    end
  end
end
