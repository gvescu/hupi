require Rails.root.join('test/support/pages/customer/rg_documents_edit.rb')

module Pages
  module Admin
    class RgDocumentsEdit < Pages::Customer::RgDocumentsEdit
      def initialize(page, user)
        @user = user
        super(page)
      end

      def visit
        @page.visit("/administracao/usuarios/#{@user.id}/rg/editar")
      end

      def click_on_cancel
        @page.click_on(t('application.rg_documents.edit.cancel'))
      end
    end
  end
end
