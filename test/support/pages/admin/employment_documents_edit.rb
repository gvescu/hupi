require Rails.root.join('test/support/pages/customer/employment_documents_edit.rb')

module Pages
  module Admin
    class EmploymentDocumentsEdit < Pages::Customer::EmploymentDocumentsEdit
      def initialize(page, user)
        @user = user
        super(page)
      end

      def visit
        @page.visit("/administracao/usuarios/#{@user.id}/carteira_de_trabalho/editar")
      end

      def click_on_cancel
        @page.click_on(t('application.employment_documents.edit.cancel'))
      end
    end
  end
end
