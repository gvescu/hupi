require Rails.root.join('test', 'support', 'pages', 'customer', 'users_edit.rb')

module Pages
  module Admin
    class UsersEdit < Pages::Customer::UsersEdit
      def initialize(page, user)
        @user = user
        super(page)
      end

      def visit
        @page.visit("/administracao/usuarios/#{@user.id}/editar")
      end

      def click_on_cancel
        @page.click_on(t('application.users.edit.cancel'))
      end
    end
  end
end
