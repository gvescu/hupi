require Rails.root.join('test/support/pages/customer/residence_documents_edit.rb')

module Pages
  module Admin
    class ResidenceDocumentsEdit < Pages::Customer::ResidenceDocumentsEdit
      def initialize(page, user)
        @user = user
        super(page)
      end

      def visit
        @page.visit("/administracao/usuarios/#{@user.id}/comprovante_de_residencia/editar")
      end

      def click_on_cancel
        @page.click_on(t('application.residence_documents.edit.cancel'))
      end
    end
  end
end
