module Pages
  module Admin
    class SessionsNew < PageObject
      def visit
        @page.visit('/administracao/entrar')
      end

      def shown?
        @page.has_content?(t('devise.signin'))
      end

      def error_message_is_shown?
        @page.has_content?(t('devise.failure.invalid'))
      end

      def fill_in_email(email)
        @page.fill_in('user[email]', with: email)
      end

      def fill_in_password(password)
        @page.fill_in('user[password]', with: password)
      end

      def click_on_log_in
        @page.click_on(t('devise.signin'))
      end
    end
  end
end
