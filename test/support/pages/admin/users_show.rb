module Pages
  module Admin
    class UsersShow < PageObject
      def initialize(page, user)
        @user = user
        super(page)
      end

      def visit
        @page.visit("/administracao/usuarios/#{@user.id}")
      end

      def shown?
        @page.has_content?(t('admin.users.show.title'))
      end

      def attach_contract_and_approve_registration(file_path)
        @page.execute_script("$('.js-contract-form input').removeClass('hidden')")
        @page.attach_file('file', file_path)
      end

      def click_on_back
        @page.click_on(t('admin.users.show.back'))
      end

      def click_on_edit_user
        @page.click_on(t('admin.users.show.edit'))
      end

      def click_on_visualize(document_name)
        button_label = t('admin.users.show.visualize')
        row_for(document_name).find('a', text: button_label).click
      end

      def click_on_edit(document_name)
        button_label = t('admin.users.show.edit')
        row_for(document_name).find('a', text: button_label).click
      end

      def click_on_approve
        @page.click_on(t('admin.users.show.approve'))
      end

      def click_on_refuse
        @page.click_on(t('admin.users.show.refuse'))
      end

      def click_on_delete
        @page.click_on(t('admin.users.show.delete'))
      end

      def has_user_information?
        @page.has_field?(User.human_attribute_name(:name), with: @user.name) &&
          @page.has_field?(User.human_attribute_name(:rg), with: @user.rg) &&
          @page.has_field?(Address.human_attribute_name(:street), with: @user.address.street) &&
          @page.has_field?(Address.human_attribute_name(:number), with: @user.address.number)
      end

      def has_no_documents_message?
        @page.has_content?(t('admin.users.show.no_documents_message'))
      end

      def has_no_approve_button?
        @page.has_no_link?(t('admin.users.show.approve'))
      end

      def has_no_refuse_button?
        @page.has_no_link?(t('admin.users.show.refuse'))
      end

      def has_document?(document_name)
        @page.has_content?(t("admin.users.show.documents.#{document_name}"))
      end

      def opened_file?(file_name)
        @page.response_headers['Content-Disposition'].match('inline') &&
          @page.response_headers['Content-Disposition'].match(file_name)
      end

      private

      def row_for(document_name)
        @page.find('tr', text: t("admin.users.show.documents.#{document_name}"))
      end
    end
  end
end
