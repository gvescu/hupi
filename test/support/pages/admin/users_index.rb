module Pages
  module Admin
    class UsersIndex < PageObject
      attr_reader :navbar

      delegate :click_on_log_out, to: :navbar

      def initialize(page)
        @navbar = PageComponents::Navbar.new(page)
        super
      end

      def visit
        @page.visit('/administracao/usuarios')
      end

      def shown?
        @page.has_content?(t('admin.users.index.title'))
      end

      def has_no_results_messages?(count:)
        @page.has_content?(t('admin.users.users.no_results'), count: count)
      end

      def has_user?(user)
        @page.has_content?(user.name) &&
          @page.has_content?(user.email) &&
          @page.has_content?(user.phone)
      end

      def has_no_user?(user)
        @page.has_no_content?(user.name)
      end

      def has_approved_message?
        @page.has_content?(t('admin.contracts.create.notice'))
      end

      def has_refused_message?
        @page.has_content?(t('admin.users.refuse.notice'))
      end

      def has_deleted_message?
        @page.has_content?(t('admin.users.destroy.notice'))
      end

      def within_table(kind)
        table = @page.find('.panel', text: t("admin.users.index.table.#{kind}")).find('table')
        @page.within(table) { yield }
      end
    end
  end
end
