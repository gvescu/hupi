module Pages
  module LandingPage
    class TermsOfService < PageObject
      def visit
        @page.visit('/legal/termos_de_uso')
      end

      def shown?
        @page.has_content?(t('landing_page.terms_of_service.title'))
      end
    end
  end
end
