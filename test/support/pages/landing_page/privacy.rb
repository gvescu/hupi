module Pages
  module LandingPage
    class Privacy < PageObject
      def visit
        @page.visit('/legal/privacidade')
      end

      def shown?
        @page.has_content?(t('landing_page.privacy.title'))
      end
    end
  end
end
