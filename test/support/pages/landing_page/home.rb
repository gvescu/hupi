module Pages
  module LandingPage
    class Home < PageObject
      def visit
        @page.visit('/')
      end

      def shown?
        @page.has_link?(t('landing_page.title')) &&
          @page.has_link?(t('landing_page.navbar.education_spots')) &&
          @page.has_link?(t('landing_page.navbar.simulation')) &&
          @page.has_link?(t('landing_page.navbar.log_in'))
      end
    end
  end
end
