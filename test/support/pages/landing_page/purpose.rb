module Pages
  module LandingPage
    class Purpose < PageObject
      def visit
        @page.visit('/proposito')
      end

      def shown?
        @page.has_content?(t('landing_page.purpose.title'))
      end
    end
  end
end
