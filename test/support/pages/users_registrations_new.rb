module Pages
  class UsersRegistrationsNew < PageObject
    def visit
      @page.visit('/usuarios/cadastrar')
    end

    def shown?
      @page.has_content?(/#{I18n.t('devise.signup')}/i)
    end

    def fill_in_email(email)
      @page.fill_in('user[email]', with: email)
    end

    def fill_in_password(password)
      @page.fill_in('user[password]', with: password)
    end

    def fill_in_password_confirmation(password)
      @page.fill_in('user[password_confirmation]', with: password)
    end

    def click_on_register
      @page.click_on(t('devise.signup'))
    end
  end
end
