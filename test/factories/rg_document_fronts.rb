FactoryGirl.define do
  factory :rg_document_front do
    type 'RgDocumentFront'
    user
    file Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'test.pdf'))
  end
end
