FactoryGirl.define do
  factory :residence_document do
    type 'ResidenceDocument'
    user
    file Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'test.pdf'))
  end
end
