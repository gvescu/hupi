FactoryGirl.define do
  factory :employment_document do
    type 'EmploymentDocument'
    user
    file Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'test.pdf'))
  end
end
