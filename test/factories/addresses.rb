FactoryGirl.define do
  factory :address do
    number { Faker::Address.building_number }
    street { Faker::Address.street_name }
    zip_code { Faker::Address.zip }
    complement { Faker::Address.secondary_address }
    neighborhood 'Bairro'
    city { Faker::Address.city }
    state { Constants::STATES.keys.sample }
  end
end
