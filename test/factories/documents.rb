FactoryGirl.define do
  factory :document do
    user
    file Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'test.pdf'))
  end
end
