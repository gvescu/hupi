FactoryGirl.define do
  factory :contract do
    type 'Contract'
    user
    file Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'test.pdf'))
  end
end
