FactoryGirl.define do
  factory :user do
    account_kit_id { Faker::Number.number(10) }
    email { Faker::Internet.unique.safe_email }
    name { Faker::Name.name }
    phone { Faker::PhoneNumber.cell_phone }
    rg { rand(1_000_000..9_999_999).to_s }
    cpf { CPF.generate }
    pis { PIS.generate }
    birthdate { Faker::Time.between(100.years.ago, Time.zone.now) }
    family_income 2000

    factory :user_with_address do
      after(:build) { |user| user.build_address(attributes_for(:address)) }

      factory :unreviewed_user do
        current_step :contract
        rg_document_front
        residence_document
        employment_document
        contract
      end
    end

    factory :admin do
      admin true
    end
  end
end
