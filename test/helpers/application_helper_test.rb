require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "#app_name returns app's name" do
    assert_equal 'HUPI', app_name
  end
end
