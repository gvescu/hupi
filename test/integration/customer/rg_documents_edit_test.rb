require 'test_helper'

module Customer
  class RgDocumentsEditTest < ActionDispatch::IntegrationTest
    test 'error message after sending empty form' do
      user = create(:user_with_address, current_step: :rg_document)

      sign_in(user)
      rg_document_edit_page.visit

      assert rg_document_edit_page.shown?

      rg_document_edit_page.click_submit

      assert rg_document_edit_page.shown?
      assert rg_document_edit_page.has_errors?
    end

    test 'redirect user if current step is not this page' do
      user = create(:user, current_step: :data)

      sign_in(user)
      rg_document_edit_page.visit

      assert user_edit_page.shown?
    end

    private

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Customer::RgDocumentsEdit.new(page)
    end

    def user_edit_page
      @user_edit_page ||= Pages::Customer::UsersEdit.new(page)
    end
  end
end
