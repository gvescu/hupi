require 'test_helper'

module Customer
  class SessionsNewTest < ActionDispatch::IntegrationTest
    test 'user logging into the system and logging out' do
      user = create(:user)
      stub_account_kit(user.account_kit_id, user.phone)

      login_page.visit

      assert login_page.shown?

      login_page.fill_in_phone(user.phone)
      login_page.click_on_log_in

      assert user_edit_page.shown?

      user_edit_page.click_on_log_out

      assert home_page.shown?
    end

    private

    def login_page
      @login_page ||= Pages::Customer::SessionsNew.new(page)
    end

    def user_edit_page
      @user_edit_page ||= Pages::Customer::UsersEdit.new(page)
    end

    def home_page
      @home_page || Pages::LandingPage::Home.new(page)
    end
  end
end
