require 'test_helper'

module Customer
  class UsersEditTest < ActionDispatch::IntegrationTest
    test 'errors after sending incorrect data' do
      user = User.create!(account_kit_id: '123', phone: '47123456789')

      sign_in(user)
      user_edit_page.visit

      assert user_edit_page.shown?

      user_edit_page.click_on_save

      assert user_edit_page.shown?
      assert user_edit_page.has_errors?(count: 13)
      assert user_edit_page.has_blank_error?
    end

    test 'redirect user if current step is not this page' do
      user = create(:user_with_address, current_step: :rg_document)

      sign_in(user)
      user_edit_page.visit

      assert rg_document_edit_page.shown?
    end

    private

    def user_edit_page
      @user_edit_page ||= Pages::Customer::UsersEdit.new(page)
    end

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Customer::RgDocumentsEdit.new(page)
    end
  end
end
