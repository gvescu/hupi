require 'test_helper'

module Customer
  class ResidenceDocumentsEditTest < ActionDispatch::IntegrationTest
    test 'error message after sending empty form' do
      user = create(:user_with_address, current_step: :residence_document)

      sign_in(user)
      residence_document_edit_page.visit

      assert residence_document_edit_page.shown?

      residence_document_edit_page.click_submit

      assert residence_document_edit_page.shown?
      assert residence_document_edit_page.has_errors?
    end

    test 'redirect user if current step is not this page' do
      user = create(:user_with_address, current_step: :rg_document)

      sign_in(user)
      residence_document_edit_page.visit

      assert rg_document_edit_page.shown?
    end

    private

    def residence_document_edit_page
      @residence_document_edit_page ||= Pages::Customer::ResidenceDocumentsEdit.new(page)
    end

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Customer::RgDocumentsEdit.new(page)
    end
  end
end
