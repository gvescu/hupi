require 'test_helper'

module Customer
  class ContractsShowTest < ActionDispatch::IntegrationTest
    test 'unreviewed message if user data is being reviewed' do
      @user = create(:user_with_address, current_step: :contract, status: :unreviewed)

      sign_in(@user)
      contracts_show_page.visit

      assert contracts_show_page.has_unreviewed_message?
    end

    test 'refused message if user data was refused' do
      @user = create(:user_with_address, current_step: :contract, status: :refused)

      sign_in(@user)
      contracts_show_page.visit

      assert contracts_show_page.has_refused_message?
    end

    test 'accepted message and download contract if user data was accepted' do
      contract = build(:contract)
      @user = create(:user_with_address, current_step: :contract, status: :accepted, contract: contract)

      sign_in(@user)
      contracts_show_page.visit

      assert contracts_show_page.has_accepted_message?
    end

    test 'redirect user if current step is not this page' do
      @user = create(:user_with_address, current_step: :rg_document)

      sign_in(@user)
      contracts_show_page.visit

      assert rg_document_edit_page.shown?
    end

    private

    def contracts_show_page
      @contracts_show_page ||= Pages::Customer::ContractsShow.new(page, @user)
    end

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Customer::RgDocumentsEdit.new(page)
    end
  end
end
