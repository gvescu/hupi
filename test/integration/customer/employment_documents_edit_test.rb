require 'test_helper'

module Customer
  class EmploymentDocumentsEditTest < ActionDispatch::IntegrationTest
    test 'error message after sending empty form' do
      user = create(:user_with_address, current_step: :employment_document)

      sign_in(user)
      employment_document_edit_page.visit

      assert employment_document_edit_page.shown?

      employment_document_edit_page.click_submit

      assert employment_document_edit_page.shown?
      assert employment_document_edit_page.has_errors?
    end

    test 'redirect user if current step is not this page' do
      user = create(:user_with_address, current_step: :rg_document)

      sign_in(user)
      employment_document_edit_page.visit

      assert rg_document_edit_page.shown?
    end

    private

    def employment_document_edit_page
      @employment_document_edit_page ||= Pages::Customer::EmploymentDocumentsEdit.new(page)
    end

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Customer::RgDocumentsEdit.new(page)
    end
  end
end
