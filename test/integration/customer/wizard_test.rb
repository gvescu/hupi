require 'test_helper'

module Customer
  class WizardTest < ActionDispatch::IntegrationTest
    test 'wizard going through each step' do
      user = log_in_and_return_user

      assert user_edit_page.shown?

      fill_and_submit_user_edit_page
      fill_and_submit_rg_document_edit_page
      fill_and_submit_residence_document_edit_page
      fill_and_submit_employment_document_edit_page
      user.reload

      assert_equal 'John McCartney', user.name
      assert_equal 'john@mail.com', user.email
      assert_equal '6012930', user.rg
      assert_equal '45683361478', user.cpf
      assert_equal '80161109100', user.pis
      assert_match '1992-12-25', user.birthdate.to_s
      assert_equal 'married', user.marital_status
      assert_equal 2000, user.family_income
      assert_equal 'Rua Benjamin Constant', user.address.street
      assert_equal 1250, user.address.number
      assert_equal '89123-200', user.address.zip_code
      assert_equal 'Apt. 1', user.address.complement
      assert_equal 'Costa e Silva', user.address.neighborhood
      assert_match 'test.pdf', user.rg_document.file.to_s
      assert_match 'test.pdf', user.residence_document.file.to_s
      assert_match 'test.pdf', user.employment_document.file.to_s
    end

    test 'user restoring wizard on rg document page' do
      user = log_in_and_return_user

      assert user_edit_page.shown?

      fill_and_submit_user_edit_page
      log_out
      log_in_with(user)

      assert rg_document_edit_page.shown?
    end

    test 'user restoring wizard on residence document page' do
      user = log_in_and_return_user

      assert user_edit_page.shown?

      fill_and_submit_user_edit_page
      fill_and_submit_rg_document_edit_page
      log_out
      log_in_with(user)

      assert residence_document_edit_page.shown?
    end

    test 'user restoring wizard on employment document page' do
      user = log_in_and_return_user

      assert user_edit_page.shown?

      fill_and_submit_user_edit_page
      fill_and_submit_rg_document_edit_page
      fill_and_submit_residence_document_edit_page
      log_out
      log_in_with(user)

      assert employment_document_edit_page.shown?
    end

    test 'user restoring wizard on contract page' do
      @user = log_in_and_return_user

      assert user_edit_page.shown?

      fill_and_submit_user_edit_page
      fill_and_submit_rg_document_edit_page
      fill_and_submit_residence_document_edit_page
      fill_and_submit_employment_document_edit_page
      log_out
      log_in_with(@user)

      assert contract_page.shown?
    end

    private

    def log_in_and_return_user
      User.create!(account_kit_id: '123', phone: '47123456789').tap do |user|
        stub_account_kit(user.account_kit_id, user.phone)
        log_in_with(user)
      end
    end

    def log_in_with(user)
      login_page.visit

      assert login_page.shown?

      login_page.fill_in_phone(user.phone)
      login_page.click_on_log_in
    end

    def log_out
      navbar.click_on_log_out
    end

    # rubocop:disable Metrics/AbcSize
    def fill_and_submit_user_edit_page
      user_edit_page.fill_name('John McCartney')
      user_edit_page.fill_email('john@mail.com')
      user_edit_page.fill_rg('6012930')
      user_edit_page.fill_cpf('45683361478')
      user_edit_page.fill_pis('80161109100')
      user_edit_page.fill_birthdate('25', 'Dezembro', '1992')
      user_edit_page.select_marital_status('married')
      user_edit_page.fill_family_income(2000)
      user_edit_page.fill_street('Rua Benjamin Constant')
      user_edit_page.fill_number('1250')
      user_edit_page.fill_zip_code('89123-200')
      user_edit_page.fill_complement('Apt. 1')
      user_edit_page.fill_neighborhood('Costa e Silva')
      user_edit_page.click_on_save

      assert rg_document_edit_page.shown?
    end
    # rubocop:enable Metrics/AbcSize

    def fill_and_submit_rg_document_edit_page
      file = Rails.root.join('test/fixtures/files/test.pdf')
      rg_document_edit_page.select_file(file)
      rg_document_edit_page.click_submit

      assert residence_document_edit_page.shown?
    end

    def fill_and_submit_residence_document_edit_page
      file = Rails.root.join('test/fixtures/files/test.pdf')
      residence_document_edit_page.select_file(file)
      residence_document_edit_page.click_submit

      assert employment_document_edit_page.shown?
    end

    def fill_and_submit_employment_document_edit_page
      file = Rails.root.join('test/fixtures/files/test.pdf')
      employment_document_edit_page.select_file(file)
      employment_document_edit_page.click_submit

      assert contract_page.shown?
    end

    def login_page
      @login_page ||= Pages::Customer::SessionsNew.new(page)
    end

    def user_edit_page
      @user_edit_page ||= Pages::Customer::UsersEdit.new(page)
    end

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Customer::RgDocumentsEdit.new(page)
    end

    def residence_document_edit_page
      @residence_document_edit_page ||= Pages::Customer::ResidenceDocumentsEdit.new(page)
    end

    def employment_document_edit_page
      @employment_document_edit_page ||= Pages::Customer::EmploymentDocumentsEdit.new(page)
    end

    def contract_page
      @contract_page ||= Pages::Customer::ContractsShow.new(page, @user)
    end

    def navbar
      @navbar ||= PageComponents::Navbar.new(page)
    end
  end
end
