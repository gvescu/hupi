require 'test_helper'

module Admin
  class EmploymentDocumentsEditTest < ActionDispatch::IntegrationTest
    test "edit user's employment document" do
      new_file = Rails.root.join('test/fixtures/files/test2.pdf')
      setup_user
      sign_in(create(:admin))

      employment_document_edit_page.visit

      assert employment_document_edit_page.shown?

      employment_document_edit_page.select_file(new_file)
      employment_document_edit_page.click_submit

      assert user_show_page.shown?

      user_show_page.click_on_visualize(:employment)

      assert user_show_page.opened_file?('test2.pdf')
    end

    test 'error message after sending empty form' do
      setup_user
      sign_in(create(:admin))

      employment_document_edit_page.visit

      assert employment_document_edit_page.shown?

      employment_document_edit_page.click_submit

      assert employment_document_edit_page.shown?
      assert employment_document_edit_page.has_errors?
    end

    test 'cancel button returns admin to user page' do
      setup_user
      sign_in(create(:admin))

      employment_document_edit_page.visit

      assert employment_document_edit_page.shown?

      employment_document_edit_page.click_on_cancel

      assert user_show_page.shown?
    end

    private

    def setup_user
      @user = create(:user_with_address, current_step: :contract)
      file = Rack::Test::UploadedFile.new(Rails.root.join('test/fixtures/files/test.pdf'))
      create(:employment_document, user: @user, file: file)
    end

    def employment_document_edit_page
      @employment_document_edit_page ||= Pages::Admin::EmploymentDocumentsEdit.new(page, @user)
    end

    def user_show_page
      @user_show_page ||= Pages::Admin::UsersShow.new(page, @user)
    end
  end
end
