require 'test_helper'

module Admin
  class RgDocumentsEditTest < ActionDispatch::IntegrationTest
    test "edit user's RG document" do
      new_file = Rails.root.join('test/fixtures/files/test2.pdf')
      setup_user
      sign_in(create(:admin))

      rg_document_edit_page.visit

      assert rg_document_edit_page.shown?

      rg_document_edit_page.select_file(new_file)
      rg_document_edit_page.click_submit

      assert user_show_page.shown?

      user_show_page.click_on_visualize(:rg)

      assert user_show_page.opened_file?('test2.pdf')
    end

    test 'error message after sending empty form' do
      setup_user
      sign_in(create(:admin))

      rg_document_edit_page.visit

      assert rg_document_edit_page.shown?

      rg_document_edit_page.click_submit

      assert rg_document_edit_page.shown?
      assert rg_document_edit_page.has_errors?
    end

    test 'cancel button returns admin to user page' do
      setup_user
      sign_in(create(:admin))

      rg_document_edit_page.visit

      assert rg_document_edit_page.shown?

      rg_document_edit_page.click_on_cancel

      assert user_show_page.shown?
    end

    private

    def setup_user
      @user = create(:user_with_address, current_step: :contract)
      file = Rack::Test::UploadedFile.new(Rails.root.join('test/fixtures/files/test.pdf'))
      create(:rg_document, user: @user, file: file)
    end

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Admin::RgDocumentsEdit.new(page, @user)
    end

    def user_show_page
      @user_show_page ||= Pages::Admin::UsersShow.new(page, @user)
    end
  end
end
