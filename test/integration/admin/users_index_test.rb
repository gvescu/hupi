require 'test_helper'

module Admin
  class UsersIndexTest < ActionDispatch::IntegrationTest
    test 'first table showing unreviewed users in contract step' do
      user1 = create(:user_with_address, status: :unreviewed, current_step: :contract)
      user2 = create(:user, status: :unreviewed, current_step: :data)

      sign_in(create(:admin))
      index_page.visit

      index_page.within_table(:unreviewed) do
        assert index_page.has_user?(user1)
        assert index_page.has_no_user?(user2)
      end
    end

    test 'second table showing unreviewed users with incomplete registration' do
      user1 = create(:user, status: :unreviewed, current_step: :data)
      user2 = create(:user_with_address, status: :unreviewed, current_step: :contract)

      sign_in(create(:admin))
      index_page.visit

      index_page.within_table(:incomplete) do
        assert index_page.has_user?(user1)
        assert index_page.has_no_user?(user2)
      end
    end

    test 'third table showing accepted users' do
      user1 = create(:user_with_address, status: :accepted)
      user2 = create(:user_with_address, status: :unreviewed, current_step: :contract)

      sign_in(create(:admin))
      index_page.visit

      index_page.within_table(:accepted) do
        assert index_page.has_user?(user1)
        assert index_page.has_no_user?(user2)
      end
    end

    test 'fourth table showing refused users' do
      user1 = create(:user, status: :refused)
      user2 = create(:user, status: :accepted)

      sign_in(create(:admin))
      index_page.visit

      index_page.within_table(:refused) do
        assert index_page.has_user?(user1)
        assert index_page.has_no_user?(user2)
      end
    end

    test 'shows registered non-admin users' do
      user1 = create(:user)
      user2 = create(:user)
      admin = create(:admin)

      sign_in(admin)
      index_page.visit

      assert index_page.has_user?(user1)
      assert index_page.has_user?(user2)
      assert index_page.has_no_user?(admin)
    end

    test 'show message if no results are shown' do
      sign_in(create(:admin))
      index_page.visit

      assert index_page.has_no_results_messages?(count: 4)
    end

    private

    def index_page
      @index_page ||= Pages::Admin::UsersIndex.new(page)
    end
  end
end
