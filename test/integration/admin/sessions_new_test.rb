require 'test_helper'

module Admin
  class SessionsNewTest < ActionDispatch::IntegrationTest
    test 'admin logging into the system and logging out' do
      user = create(:admin, password: 'nopasswd')

      login_page.visit

      assert login_page.shown?

      login_page.fill_in_email(user.email)
      login_page.fill_in_password('nopasswd')
      login_page.click_on_log_in

      assert users_index_page.shown?

      users_index_page.click_on_log_out

      assert home_page.shown?
    end

    test 'error message when email and password does not match' do
      login_page.visit

      assert login_page.shown?

      login_page.fill_in_email('wrong@email.com')
      login_page.fill_in_password('wrongpasswd')
      login_page.click_on_log_in

      assert login_page.error_message_is_shown?
    end

    private

    def login_page
      @login_page ||= Pages::Admin::SessionsNew.new(page)
    end

    def users_index_page
      @users_index_page ||= Pages::Admin::UsersIndex.new(page)
    end

    def home_page
      @home_page || Pages::LandingPage::Home.new(page)
    end
  end
end
