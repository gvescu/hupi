require 'test_helper'

module Admin
  class UsersShowTest < ActionDispatch::IntegrationTest
    test 'show user information' do
      @user = create(:user_with_address)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_user_information?
    end

    test 'show and visualize rg document' do
      @user = create(:user, rg_document: build(:rg_document))
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_document?('rg')

      show_page.click_on_visualize('rg')

      assert show_page.opened_file?('test.pdf')
    end

    test 'go to rg document edit page' do
      @user = create(:user, rg_document: build(:rg_document))
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_document?('rg')

      show_page.click_on_edit('rg')

      assert rg_document_edit_page.shown?
    end

    test 'show and visualize residence document' do
      @user = create(:user, residence_document: build(:residence_document))
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_document?('residence')

      show_page.click_on_visualize('residence')

      assert show_page.opened_file?('test.pdf')
    end

    test 'go to residence document edit page' do
      @user = create(:user, residence_document: build(:residence_document))
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_document?('residence')

      show_page.click_on_edit('residence')

      assert residence_document_edit_page.shown?
    end

    test 'show and visualize employment document' do
      @user = create(:user, employment_document: build(:employment_document))
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_document?('employment')

      show_page.click_on_visualize('employment')

      assert show_page.opened_file?('test.pdf')
    end

    test 'go to employment document edit page' do
      @user = create(:user, employment_document: build(:employment_document))
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_document?('employment')

      show_page.click_on_edit('employment')

      assert employment_document_edit_page.shown?
    end

    test 'show and visualize contract' do
      @user = create(:user, contract: build(:contract))
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_document?('contract')

      show_page.click_on_visualize('contract')

      assert show_page.opened_file?('test.pdf')
    end

    test 'go to user edit page' do
      @user = create(:user)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?

      show_page.click_on_edit_user

      assert edit_page.shown?
    end

    test 'go back to users index' do
      @user = create(:user)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?

      show_page.click_on_back

      assert index_page.shown?
    end

    test 'approve button should not be visible if user data is incomplete' do
      @user = create(:user, status: :unreviewed, current_step: :data)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_no_approve_button?
    end

    test 'approve button should not be visible if user was already accepted' do
      @user = create(:user, status: :accepted)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_no_approve_button?
    end

    test 'approve/refuse button should not be visible if user was refused' do
      @user = create(:user, status: :refused)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_no_approve_button?
      assert show_page.has_no_refuse_button?
    end

    test 'show message if user data is incomplete and he does not have anyattached document' do
      @user = create(:user, status: :unreviewed, current_step: :data)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?
      assert show_page.has_no_documents_message?
    end

    test 'approve user' do
      Capybara.current_driver = Capybara.javascript_driver
      @user = create(:unreviewed_user)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?

      show_page.click_on_approve
      show_page.attach_contract_and_approve_registration('test/fixtures/files/test.pdf')

      assert index_page.shown?
      assert index_page.has_approved_message?
    end

    test 'refuse user' do
      @user = create(:unreviewed_user)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?

      show_page.click_on_refuse

      assert index_page.shown?
      assert index_page.has_refused_message?
    end

    test 'delete user' do
      @user = create(:unreviewed_user)
      sign_in(create(:admin))

      show_page.visit

      assert show_page.shown?

      show_page.click_on_delete

      assert index_page.shown?
      assert index_page.has_deleted_message?
      assert index_page.has_no_user?(@user)
    end

    private

    def show_page
      @show_page ||= Pages::Admin::UsersShow.new(page, @user)
    end

    def index_page
      @index_page ||= Pages::Admin::UsersIndex.new(page)
    end

    def edit_page
      @edit_page ||= Pages::Admin::UsersEdit.new(page, @user)
    end

    def rg_document_edit_page
      @rg_document_edit_page ||= Pages::Admin::RgDocumentsEdit.new(page, @user)
    end

    def residence_document_edit_page
      @residence_document_edit_page ||= Pages::Admin::ResidenceDocumentsEdit.new(page, @user)
    end

    def employment_document_edit_page
      @employment_document_edit_page ||= Pages::Admin::EmploymentDocumentsEdit.new(page, @user)
    end
  end
end
