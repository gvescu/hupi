require 'test_helper'

module Admin
  class UsersEditTest < ActionDispatch::IntegrationTest
    test 'edit user data being an admin' do
      @user = create(:user)
      sign_in(create(:admin))

      user_edit_page.visit

      assert user_edit_page.shown?

      user_edit_page.fill_name('John McCartney')
      user_edit_page.fill_email('john@mail.com')
      user_edit_page.fill_rg('6012930')
      user_edit_page.fill_cpf('45683361478')
      user_edit_page.fill_pis('80161109100')
      user_edit_page.fill_birthdate('25', 'Dezembro', '1992')
      user_edit_page.select_marital_status('married')
      user_edit_page.fill_family_income(2000)
      user_edit_page.fill_street('Rua Benjamin Constant')
      user_edit_page.fill_number('1250')
      user_edit_page.fill_zip_code('89123-200')
      user_edit_page.fill_complement('Apt. 1')
      user_edit_page.fill_neighborhood('Costa e Silva')
      user_edit_page.click_on_save

      assert user_show_page.shown?
    end

    test 'errors after sending incorrect data' do
      @user = create(:user_with_address, current_step: :rg_document)

      sign_in(create(:admin))
      user_edit_page.visit

      assert user_edit_page.shown?

      user_edit_page.fill_name('')
      user_edit_page.fill_rg('')
      user_edit_page.fill_cpf('')
      user_edit_page.fill_pis('')
      user_edit_page.click_on_save

      assert user_edit_page.shown?
      assert user_edit_page.has_errors?(count: 7)
      assert user_edit_page.has_blank_error?
    end

    test 'cancel button returns admin to user page' do
      @user = create(:user_with_address, current_step: :rg_document)

      sign_in(create(:admin))
      user_edit_page.visit

      assert user_edit_page.shown?

      user_edit_page.click_on_cancel

      assert user_show_page.shown?
    end

    private

    def user_edit_page
      @user_edit_page ||= Pages::Admin::UsersEdit.new(page, @user)
    end

    def user_show_page
      @user_show_page ||= Pages::Admin::UsersShow.new(page, @user)
    end
  end
end
