require 'test_helper'

class ApplicationNavbarTest < ActionDispatch::IntegrationTest
  test 'shows navbar' do
    sign_in(create(:user))
    user_edit_page.visit

    assert navbar.shown?
  end

  private

  def user_edit_page
    @user_edit_page ||= Pages::Customer::UsersEdit.new(page)
  end

  def navbar
    @navbar ||= PageComponents::Navbar.new(page)
  end
end
