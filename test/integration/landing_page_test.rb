require 'test_helper'

class LandingPageTest < ActionDispatch::IntegrationTest
  test 'visit home page' do
    home_page.visit

    assert home_page.shown?
  end

  test 'visit terms of service' do
    terms_of_service_page.visit

    assert terms_of_service_page.shown?
  end

  test 'visit privacy' do
    privacy_page.visit

    assert privacy_page.shown?
  end

  test 'visit purpose page' do
    purpose_page.visit

    assert purpose_page.shown?
  end

  private

  def home_page
    @home_page ||= Pages::LandingPage::Home.new(page)
  end

  def terms_of_service_page
    @terms_of_service_page ||= Pages::LandingPage::TermsOfService.new(page)
  end

  def privacy_page
    @privacy_page ||= Pages::LandingPage::Privacy.new(page)
  end

  def purpose_page
    @purpose_page ||= Pages::LandingPage::Purpose.new(page)
  end
end
