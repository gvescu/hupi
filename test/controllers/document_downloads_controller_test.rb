require 'test_helper'

class DocumentDownloadsControllerTest < ActionDispatch::IntegrationTest
  test 'user should be able to download his document' do
    user = create(:user)
    document = create(:rg_document, user: user)

    sign_in(user)
    get document_download_path(document)

    assert response.success?
    assert_equal document.file.content_type, response.content_type
    assert_match 'inline', response.headers['Content-Disposition']
    assert_match File.basename(document.file.path), response.headers['Content-Disposition']
  end

  test 'user should not be able to download documents from other user' do
    user = create(:user)
    document = create(:rg_document)

    sign_in(user)
    assert_raises(ActiveRecord::RecordNotFound) { get document_download_path(document) }
  end

  test 'admin should be able to download any document' do
    user = create(:admin)
    document = create(:rg_document)

    sign_in(user)
    get document_download_path(document)

    assert response.success?
    assert_equal document.file.content_type, response.content_type
    assert_match 'inline', response.headers['Content-Disposition']
    assert_match File.basename(document.file.path), response.headers['Content-Disposition']
  end
end
