require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test '#create returns error if state does not match' do
    post user_session_path, params: { state: 'wrong' }

    assert_response :bad_request
  end

  test '#create creates user on first sign in' do
    stub_account_kit('123', '47123456789')

    assert_difference 'User.count', 1 do
      post user_session_path, params: { code: 'code' }
    end

    assert User.exists?(account_kit_id: '123', phone: '47123456789')
    assert_redirected_to edit_user_path
  end

  test '#create signs in existent user' do
    user = create(:user)
    stub_account_kit(user.account_kit_id, user.phone)

    assert_difference 'User.count', 0 do
      post user_session_path, params: { code: 'code' }
    end

    assert_redirected_to edit_user_path
  end

  test '#create updates phone number from existing user' do
    user = create(:user, phone: '00000000000')
    stub_account_kit(user.account_kit_id, '11111111111')

    post user_session_path, params: { code: 'code' }

    assert_equal '11111111111', user.reload.phone
  end

  test '#create returns error if account kit return unexpected error' do
    stub_account_kit(nil, nil)

    post user_session_path, params: { code: 'wrong' }

    assert_response :bad_request
  end
end
