require 'test_helper'

module Admin
  class ContractsControllerTest < ActionDispatch::IntegrationTest
    def setup
      file_path = Rails.root.join('test/fixtures/files/test.pdf')
      file = Rack::Test::UploadedFile.new(file_path)

      @user = create(:user_with_address, current_step: :contract, status: :unreviewed)
      @params = { user_id: @user.id, file: file }

      sign_in(create(:admin))
    end

    test '#create creates contract' do
      assert_difference 'Contract.count', 1 do
        post admin_contracts_path, params: @params
      end

      assert_not_nil @user.reload.contract
    end

    test '#create redirects user to user list page' do
      post admin_contracts_path, params: @params

      assert_redirected_to admin_users_path
    end

    test '#create updates user status to accepted' do
      post admin_contracts_path, params: @params

      assert_equal 'accepted', @user.reload.status
    end

    test '#create sends email to user' do
      post admin_contracts_path, params: @params

      assert_equal 1, enqueued_jobs.size
      assert_equal 'ContractMailer', enqueued_jobs.first[:args].first
      assert_equal 'deliver_contract', enqueued_jobs.first[:args].second
    end
  end
end
