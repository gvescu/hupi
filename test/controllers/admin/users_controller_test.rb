require 'test_helper'

module Admin
  class UsersControllerTest < ActionDispatch::IntegrationTest
    test '#index returns unauthorized error if user is not admin' do
      sign_in(create(:user))

      get admin_users_path

      assert response.unauthorized?
    end

    test '#index returns success if user is admin' do
      sign_in(create(:admin))

      get admin_users_path

      assert response.success?
    end

    test '#show returns unauthorized error if user is not admin' do
      user = create(:user)
      sign_in(create(:user))

      get admin_user_path(user)

      assert response.unauthorized?
    end

    test '#show returns success if user is admin' do
      user = create(:user)
      sign_in(create(:admin))

      get admin_user_path(user)

      assert response.success?
    end

    test '#refuse marks user as refused' do
      user = create(:user_with_address, current_step: :contract, status: :unreviewed)
      sign_in(create(:admin))

      delete refuse_admin_user_path(user)

      assert_equal 'refused', user.reload.status
    end

    test '#refuse redirects admin to user list' do
      user = create(:user_with_address, current_step: :contract, status: :unreviewed)
      sign_in(create(:admin))

      delete refuse_admin_user_path(user)

      assert_redirected_to admin_users_path
    end

    test '#refuse sends refused message to user' do
      user = create(:user_with_address, current_step: :contract, status: :unreviewed)
      sign_in(create(:admin))

      delete refuse_admin_user_path(user)

      assert_equal 1, enqueued_jobs.size
      assert_equal 'UserMailer', enqueued_jobs.first[:args].first
      assert_equal 'send_refuse_message', enqueued_jobs.first[:args].second
    end
  end
end
