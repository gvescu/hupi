require 'test_helper'

class PISTest < ActiveSupport::TestCase
  test '.generate creates a valid PIS number' do
    pis = PIS.generate

    assert_equal 11, pis.size
    assert PIS.valid?(pis)
  end

  test '.generate creates a valid PIS numbers and returns it formatted' do
    pis = PIS.generate(true)

    assert_equal 14, pis.size
    assert_match(/\A(\d{3})\.(\d{5})\.(\d{2})-\d\z/, pis)
    assert PIS.valid?(pis)
  end

  test '.generate_verifier_digit creates a verifier digit using PIS formula' do
    numbers = [2, 1, 0, 9, 3, 8, 4, 9, 2, 4]
    assert_equal 1, PIS.generate_verifier_digit(numbers)
  end

  test '.generate_verifier_digit considers verifier digits bigger than 10' do
    numbers = [1, 0, 1, 2, 9, 6, 5, 9, 6, 2]
    assert_equal 0, PIS.generate_verifier_digit(numbers)
  end

  test '.valid? instantiate object and returns #valid? result' do
    assert PIS.valid?('21093849241')
  end

  test '#valid? returns false if PIS is blacklisted' do
    refute PIS.new('0000000000000').valid?
  end

  test '#valid? returns false if PIS does not have 11 digits' do
    refute PIS.new('000').valid?
  end

  test '#valid? returns false if PIS is invalid' do
    refute PIS.new('12312333312').valid?
  end

  test '#valid? returns true if PIS is valid' do
    assert PIS.new('21093849241').valid?
  end

  test '#valid? accepts numbers with punctuation' do
    assert PIS.new('210.93849.24-1').valid?
  end

  test '#formatted returns formatted PIS' do
    assert_equal '210.93849.24-1', PIS.new('21093849241').formatted
  end
end
