require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  test 'valid address' do
    address = Address.new(
      number: 123,
      street: 'Rua Benjamin Constant',
      zip_code: '89217-100',
      complement: 'Apt. 1',
      city: 'Joinville',
      state: 'SC',
      user: create(:user)
    )

    assert address.valid?
  end

  test 'invalid address without number' do
    address = Address.new
    assert address.invalid?
    assert address.errors.added?(:number, :blank)
  end

  test 'invalid address without street' do
    address = Address.new
    assert address.invalid?
    assert address.errors.added?(:street, :blank)
  end

  test 'invalid address without zip_code' do
    address = Address.new
    assert address.invalid?
    assert address.errors.added?(:zip_code, :blank)
  end

  test 'invalid address without city' do
    address = Address.new
    assert address.invalid?
    assert address.errors.added?(:city, :blank)
  end

  test 'invalid address without state' do
    address = Address.new
    assert address.invalid?
    assert address.errors.added?(:state, :blank)
  end

  test 'invalid address with invalid zip code format' do
    address = Address.new(zip_code: '123')
    assert address.invalid?
    assert address.errors.added?(:zip_code, :invalid)
  end

  test 'invalid address with invalid state' do
    address = Address.new(state: 'XX')
    assert address.invalid?
    assert address.errors.added?(:state, :inclusion, value: 'XX')
  end
end
