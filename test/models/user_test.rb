require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'valid user' do
    user = User.new(account_kit_id: '123', phone: '47123456789')

    assert user.valid?
  end

  test 'valid user with waiting_for_analysis data' do
    user = User.new(
      account_kit_id: '123',
      email: 'user@email.com',
      name: 'John Doe',
      phone: '(47) 99912-3456',
      rg: '1423019',
      cpf: '58993676607',
      pis: '21093849241',
      birthdate: 24.years.ago,
      family_income: 1_000_000_000,
      marital_status: :married,
      address: build(:address),
      employment_document: build(:employment_document),
      residence_document: build(:residence_document),
      rg_document: build(:rg_document),
      contract: build(:contract)
    )

    assert user.valid?
  end

  test 'invalid user without account kit ID' do
    user = User.new

    assert user.invalid?
    assert user.errors.added?(:account_kit_id, :blank)
  end

  test 'invalid user with duplicate account kit ID' do
    create(:user, account_kit_id: '123')
    user = User.new(account_kit_id: '123')

    assert user.invalid?
    assert user.errors.added?(:account_kit_id, :taken)
  end

  test 'invalid user without phone' do
    user = User.new

    assert user.invalid?
    assert user.errors.added?(:phone, :blank)
  end

  test 'invalid user without email when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:email, :blank)
  end

  test 'invalid user without name when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:name, :blank)
  end

  test 'invalid user without phone when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:phone, :blank)
  end

  test 'invalid user without rg when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:rg, :blank)
  end

  test 'invalid user without cpf when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:cpf, :blank)
  end

  test 'invalid user without pis when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:pis, :blank)
  end

  test 'invalid user without birthdate when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:birthdate, :blank)
  end

  test 'invalid user without family income when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:family_income, :blank)
  end

  test 'invalid user without address when step is not data' do
    user = User.new(current_step: :rg_document)

    assert user.invalid?
    assert user.errors.added?(:address, :blank)
  end

  test 'invalid user without contract when step is contract and user was approved' do
    user = User.new(current_step: :contract, status: :accepted)

    assert user.invalid?
    assert user.errors.added?(:contract, :blank)
  end

  test 'invalid user with invalid email' do
    user = User.new(email: 'invalid')

    assert user.invalid?
    assert user.errors.added?(:email, :invalid)
  end

  test 'invalid user with invalid rg when step is not data' do
    user = User.new(current_step: :rg_document, rg: 'letters')

    assert user.invalid?
    assert user.errors.added?(:rg, :invalid)
  end

  test 'invalid user with invalid cpf when step is not data' do
    user = User.new(current_step: :rg_document, cpf: '12312312312')

    assert user.invalid?
    assert user.errors.added?(:cpf, :invalid)
  end

  test 'invalid user with invalid pis when step is not data' do
    user = User.new(current_step: :rg_document, pis: '12312312312')

    assert user.invalid?
    assert user.errors.added?(:pis, :invalid)
  end

  test 'user is not admin by default' do
    user = User.new

    refute user.admin?
  end

  test '.not_admin scope returns non admins' do
    user = create(:user)
    create(:admin)

    users = User.not_admin

    assert_equal 1, users.size
    refute user.admin?
  end

  test '.incomplete_and_incomplete scope returns unreviewed users not in contract step' do
    create_users_for_scope_test

    users = User.unreviewed_and_incomplete

    assert_equal 2, users.size
    assert_equal [:unreviewed, :unreviewed], users.map(&:status).map(&:to_sym)
    refute_equal :contract, users.first.current_step.to_sym
  end

  test '.unreviewed_and_complete scope returns unreviewed users in the contract step' do
    create_users_for_scope_test

    users = User.unreviewed_and_complete

    assert_equal 1, users.size
    assert_equal :unreviewed, users.first.status.to_sym
    assert_equal :contract, users.first.current_step.to_sym
  end

  test '#rg= removes any non-digit' do
    user = User.new(rg: '6-123-987')

    assert_equal '6123987', user.rg
  end

  test '#rg= allows attribute to be erased' do
    user = User.new(rg: '6-123-987')
    user.rg = nil

    assert_nil user.rg
  end

  test '#cpf= removes any non-digit' do
    user = User.new(cpf: '6-123-987')

    assert_equal '6123987', user.cpf
  end

  test '#cpf= allows attribute to be erased' do
    user = User.new(cpf: '6-123-987')
    user.cpf = nil

    assert_nil user.cpf
  end

  test '#pis= removes any non-digit' do
    user = User.new(pis: '6-123-987')

    assert_equal '6123987', user.pis
  end

  test '#pis= allows attribute to be erased' do
    user = User.new(pis: '6-123-987')
    user.pis = nil

    assert_nil user.pis
  end

  test '#phone= removes any non-digit' do
    user = User.new(phone: '(47) 12345-6789')

    assert_equal '47123456789', user.phone
  end

  test'#phone= allows attribute to be erased' do
    user = User.new(phone: '(47) 12345-6789')
    user.phone = nil

    assert_nil user.phone
  end

  private

  def create_users_for_scope_test
    create(:user, status: :unreviewed, current_step: :data)
    create(:user_with_address, status: :unreviewed, current_step: :rg_document)
    create(:user_with_address, status: :unreviewed, current_step: :contract)
    create(:user_with_address, status: :accepted)
    create(:user_with_address, status: :refused)
  end
end
