require 'test_helper'

class DocumentTest < ActiveSupport::TestCase
  test 'valid document' do
    tempfile = File.new(Rails.root.join('test', 'fixtures', 'files', 'test.pdf'))
    document = Document.new(user: create(:user), file: tempfile)
    assert document.valid?
  end

  test 'invalid document without file' do
    document = Document.new

    assert document.invalid?
    assert document.errors.added?(:file, :blank)
  end
end
