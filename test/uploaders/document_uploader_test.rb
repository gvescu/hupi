require 'test_helper'

class DocumentTest < ActiveSupport::TestCase
  test '#store_dir should be on /uploads' do
    model = OpenStruct.new(id: 123)

    assert_match 'uploads', DocumentUploader.new(model).store_dir
  end

  test '#extension_whitelist should include JPG, PNG and PDF' do
    assert_equal %w(jpg jpeg png pdf), DocumentUploader.new.extension_whitelist
  end

  test '#default_url returns fallback URL' do
    default_url = DocumentUploader.new.default_url

    assert_match %r{/assets/fallback/default.*png}, default_url
  end
end
