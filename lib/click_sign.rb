require 'base64'

class ClickSign
  DEFAULT_JSON = {
    "document": {
      "path": "/Hupi/Contratos/",
      "deadline_at": Time.now + 90.days,
      "signers": [
        {
          "email": "",
          "phone_number": "",
          "sign_as": "sign",
          "auths": [
            "email"
          ],
          "send_email": false,
          "message": "Olá, por favor assine o documento."
        }
      ]
    }
  }

  def self.validate_callback action, key, data        
    if ClickSign.validated_hmac? data      
      user = User.find_by_document_key key
      if action == 'sign' && !user.nil?        
        user.update!(signature: Time.now, current_step: :boxAnalyze, contract_approved: Time.now)
        ShopMailer.send_accept_contract(user).deliver
      end  
    end          
  end  

  def self.create_document file, filename, user
      
    DEFAULT_JSON[:document][:path] << filename
    DEFAULT_JSON[:document][:content_base64] = 'data:application/pdf;base64,' + Base64.encode64(open(file.path) { |io| io.read })
    DEFAULT_JSON[:document][:signers][0][:email] = user.email
    DEFAULT_JSON[:document][:signers][0][:phone_number] = user.phone

    req = nil    

    begin
      req = RestClient.post "#{ENV['CLICKSIGN_URL']}/documents?access_token=#{ENV['CLICKSIGN_TOKEN']}", DEFAULT_JSON.to_json, { content_type: :json, accept: :json }
    rescue RestClient::ExceptionWithResponse => e
      p e.response.body
    end  

    JSON.parse(req.body)
  end

  def self.get_document key
    req = nil

    begin
      req = RestClient.get "#{ENV['CLICKSIGN_URL']}/documents/#{key}?access_token=#{ENV['CLICKSIGN_TOKEN']}", { accept: :json }
    rescue RestClient::ExceptionWithResponse => e
      p e.response.body
    end      

    JSON.parse(req.body)
  end  

  def self.get_document_url key
    req = ClickSign.get_document key

    req['document']['signers'][0]['url']
  end  

  def self.cancel_document key
    begin
      req = RestClient.patch "#{ENV['CLICKSIGN_URL']}/documents/#{key}/cancel?access_token=#{ENV['CLICKSIGN_TOKEN']}", DEFAULT_JSON.to_json, { content_type: :json, accept: :json }
    rescue RestClient::ExceptionWithResponse => e
      p e.response.body
    end      
  end  

  def self.validated_hmac? data    
    mac = OpenSSL::HMAC.hexdigest("SHA256", ENV['CLICKSIGN_HMAC'], data.raw_post)    
    mac == data.headers["Content-Hmac"].gsub('sha256=', '')
  end  
end