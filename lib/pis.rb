class PIS
  STRICT_REGEX = /[.-]/
  FORMATTED_REGEX = /\A(\d{3})(\d{5})(\d{2})(\d)\z/
  FORMAT_STRUCTURE = '\\1.\\2.\\3-\\4'.freeze
  BLACKLIST = %w(00000000000).freeze
  WEIGHTS = [3, 2, 9, 8, 7, 6, 5, 4, 3, 2].freeze
  NUMBERS = [*0..9].freeze

  attr_reader :number

  def self.generate(formatted = false)
    numbers = Array.new(10).map { NUMBERS.sample }
    verifier_digit = generate_verifier_digit(numbers)
    pis = numbers.push(verifier_digit).join

    formatted ? PIS.new(pis).formatted : pis
  end

  def self.generate_verifier_digit(numbers)
    result = 11 - WEIGHTS.zip(numbers).map { |a, b| a * b }.sum % 11
    result >= 10 ? 0 : result
  end

  def self.valid?(number)
    new(number).valid?
  end

  def initialize(number)
    @number = number
  end

  def valid?
    return false if stripped.size != 11 || BLACKLIST.include?(stripped)
    self.class.generate_verifier_digit(numbers[0..9]) == numbers[10]
  end

  def formatted
    stripped.gsub(FORMATTED_REGEX, FORMAT_STRUCTURE)
  end

  private

  def stripped
    @stripped ||= number.to_s.gsub(STRICT_REGEX, '')
  end

  def numbers
    @numbers ||= stripped.each_char.to_a.map(&:to_i)
  end
end
