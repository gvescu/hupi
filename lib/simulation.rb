class Simulation
  def self.calculate_br lace
    lace = lace.sub(".","").sub(",",".").to_f        
    
    if lace < 1800
      {
        info: 'Renda mínima permitida é de R$ 1800,00.'
      }
    elsif lace >= 1800 && lace < 1900
      {
        apartamento: 99000,
        renda: 1800,
        subsidio: 20351.00,
        fgts: 0,
        financiamento: 78649.00,
        entrada: 0,
        primeira_parcela: 528.49
      }
    elsif lace >= 1900 && lace < 2000
      {
        apartamento: 99000,
        renda: 1900,
        subsidio: 17332.00,
        fgts: 0,
        financiamento: 81668.00,
        entrada: 0,
        primeira_parcela: 549.73
      }
    elsif lace >= 2000 && lace < 2100
      {
        apartamento: 99000,
        renda: 2000,
        subsidio: 10018.00,
        fgts: 0,
        financiamento: 88982.00,
        entrada: 0,
        primeira_parcela: 596.98
      }
    elsif lace >= 2100 && lace < 2200
      {
        apartamento: 99000,
        renda: 2100,
        subsidio: 7905.00,
        fgts: 1995.00,
        financiamento: 89100.00,
        entrada: 0,
        primeira_parcela: 621.81
      }
    elsif lace >= 2200 && lace < 2300
      {
        apartamento: 99000,
        renda: 2200,
        subsidio: 8274.00,
        fgts: 1626.00,
        financiamento: 89100.00,
        entrada: 0,
        primeira_parcela: 599.09
      }
    elsif lace >= 2300 && lace < 2400
      {
        apartamento: 99000,
        renda: 2300,
        subsidio: 5254.00,
        fgts: 4646.00,
        financiamento: 89100.00,
        entrada: 0,
        primeira_parcela: 597.76
      }
    elsif lace >= 2400 && lace < 2500
      {
        apartamento: 99000,
        renda: 2400,
        subsidio: 2621.00,
        fgts: 7279.00,
        financiamento: 89100.00,
        entrada: 0,
        primeira_parcela: 597.76
      }
    elsif lace >= 2500 && lace < 2600
      {
        apartamento: 99000,
        renda: 2500,
        subsidio: 2621.00,
        fgts: 7279.00,
        financiamento: 89100.00,
        entrada: 0,
        primeira_parcela: 597.76
      }
    elsif lace == 2600
      {
        apartamento: 99000,
        renda: 2600,
        subsidio: 3745.00,
        fgts:  6155.00,
        financiamento: 89100.00,
        entrada: 0,
        primeira_parcela: 597.76
      }
    elsif lace > 2600
      {
        info: 'Renda máxima permitida é de R$ 2600,00.'
      }
    end    
  end

  def self.calculate_py lace
    lace = lace.gsub(".","").sub(",","").to_f    

    if lace < 5000000
      {
        info: 'La renta mínima permitida es ₲ 5.000.000'
      }
    elsif lace >= 5000000 && lace < 5999999
      first_budget 5000000
    elsif lace >= 6000000 && lace < 6999999
      first_budget 6000000
    elsif lace >= 7000000 && lace < 7999999
      first_budget 7000000
    elsif lace >= 8000000 && lace < 8999999
      first_budget 8000000
    elsif lace >= 9000000 && lace < 9999999
      second_budget 9000000
    elsif lace >= 10000000 && lace < 10999999
      second_budget 10000000  
    elsif lace >= 11000000 && lace < 11999999
      second_budget 11000000    
    elsif lace == 12000000
      second_budget 12000000     
    elsif lace > 12000000
      {
        info: 'La renta máxima permitida es ₲ 12.000.000'
      }
    end    
  end

  private

  def self.first_budget renda
    {
      apartamentos: [
        {
          valor: 165000000,
          subsidio: 1462151
        },
        {
          valor: 170000000,
          subsidio: 1506458 
        },
        {
          valor: 175000000,
          subsidio: 1550766
        },
        {
          valor: 180000000,
          subsidio: 1595074
        },
        {
          valor: 185000000,
          subsidio: 1639381
        },
        {
          valor: 190000000,
          subsidio: 1683689
        },
        {
          valor: 195000000,
          subsidio: 1727997
        },
        {
          valor: 200000000,
          subsidio: 1772304
        }
      ],
      renda: renda
    }  
  end    

  def self.second_budget renda
    {
      apartamentos: [
        {
          valor: 165000000,
          subsidio: 1691817
        },
        {
          valor: 170000000,
          subsidio: 1743085 
        },
        {
          valor: 175000000,
          subsidio: 1794353
        },
        {
          valor: 180000000,
          subsidio: 1845619
        },
        {
          valor: 185000000,
          subsidio: 1896887
        },
        {
          valor: 190000000,
          subsidio: 1948153
        },
        {
          valor: 195000000,
          subsidio: 1999421
        },
        {
          valor: 200000000,
          subsidio: 2050688
        }
      ],
      renda: renda
    }  
  end    
end